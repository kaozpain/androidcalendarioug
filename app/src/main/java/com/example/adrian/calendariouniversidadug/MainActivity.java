package com.example.adrian.calendariouniversidadug;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.CalendarContract;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.adrian.calendariouniversidadug.calendar.EventosSQL;
import com.example.adrian.calendariouniversidadug.calendar.ServiceForActualization;
import com.example.adrian.calendariouniversidadug.calendar.nombreRandom;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, FragmentUno.OnFragmentInteractionListener, FragmentDos.OnFragmentInteractionListener, FragmentTres.OnFragmentInteractionListener, AgregarEvento.OnFragmentInteractionListener {

    private final String ruta_fotos = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/UgCalendarphotos/";
    private File file = new File(ruta_fotos);
    static double longitud, latitud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        SharedPreferences auxLogin=getSharedPreferences("LogeoUg", MODE_PRIVATE);
        //SharedPreferences auxLogin=getPreferences(context.MODE_PRIVATE);
        String accesoNombre=auxLogin.getString("nombre","");
        String accesoCorreo=auxLogin.getString("correo","");
        Integer accesoCedula=auxLogin.getInt("cedula",0);
        Integer accesoTipo=auxLogin.getInt("tipo",0);
        View hView = navigationView.getHeaderView(0);
        ImageView img=(ImageView) hView.findViewById(R.id.imageViewNav);

//Valida si esta registrado o no//////////////////////////////
        if (accesoNombre!=""&&accesoNombre!=null) {
            startService(new Intent(MainActivity.this, ServiceForActualization.class).putExtra("cedula", accesoCedula));
            TextView item4 = (TextView) hView.findViewById(R.id.NombreNav);
            TextView item5 = (TextView) hView.findViewById(R.id.CorreoNav);
            item4.setText(accesoNombre);
            item5.setText(accesoCorreo);

            SharedPreferences aux= PreferenceManager.getDefaultSharedPreferences(MainActivity.this);

            ImagenPortada WSllamada=new ImagenPortada();
            WSllamada.execute(img);

            FragmentUno x= new FragmentUno();
            Bundle bundle=new Bundle();
            bundle.putString("accesoNombre",accesoNombre);
            x.setArguments(bundle);
            getSupportFragmentManager().beginTransaction().replace(R.id.Contendedor,x).commit();
            /*final ImagesDownload hilo=new ImagesDownload();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    hilo.run(img);
                }
            }).start();*/
                //hilo.run(img);
                //hilo.start();
            if(aux.getBoolean("BanderaUbicacion", false))
            ObtenerUbicacion();
        }
        else {
            finish();
            Intent intent = new Intent(this, Login.class);
            startActivityForResult(intent, 0);
        }
//////////////////////////////////////////////////////////////////
        /*Bundle bundle=getIntent().getExtras();
        if(bundle!=null) {
            accesoUsuario = bundle.getString("usuario");
            accesoClave = bundle.getString("clave");
        }*/


        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                file.mkdirs();
                nombreRandom auxi=new nombreRandom();
                String file=ruta_fotos+auxi.getCode()+".jpg";
                File foto=new File(file);
                try{
                    foto.createNewFile();
                }catch (IOException e){
                    Log.e("error al guardar ", ""+e);
                }
                Uri uri=Uri.fromFile(foto);
                //se abre la camara
                Intent CameraIntent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                CameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                startActivityForResult(CameraIntent, 0);
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(new Intent(MainActivity.this, ServiceForActualization.class));
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        /*if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {
                    Intent intent = new Intent (this, SettingsActivity.class);
                    startActivityForResult(intent, 0);
        } else*/ if (id == R.id.nav_slideshow) {

            SharedPreferences auxLogin=getSharedPreferences("LogeoUg", MODE_PRIVATE);
            //SharedPreferences auxLogin=getPreferences(context.MODE_PRIVATE);
            String accesoUsuario=auxLogin.getString("usuario","");
            String accesoClave=auxLogin.getString("clave","");
            if (accesoUsuario!=""&&accesoUsuario!=null) {
                FragmentUno x = new FragmentUno();
                Bundle bundle = new Bundle();
                bundle.putString("accesoUsuario", accesoUsuario);
                bundle.putString("accesoClave", accesoClave);
                x.setArguments(bundle);
                getSupportFragmentManager().beginTransaction().replace(R.id.Contendedor, x).commit();
                }
        } else if (id == R.id.nav_manage) {
                Intent intent = new Intent (this, SettingsActivity.class);
                startActivityForResult(intent, 0);
        } else if (id == R.id.nav_share) {

            EventosSQL usdbh = new EventosSQL(this, "DBCalendar", null, 1);
            final SQLiteDatabase db = usdbh.getWritableDatabase();
            SharedPreferences aux= PreferenceManager.getDefaultSharedPreferences(this);
            Cursor cursorEventos = db.rawQuery("select fecha_inicio, fecha_fin, titulo, descripcion, id_evento from ug_eventos", null);//aqui se debe aumentar logica
            if (cursorEventos != null) {
                cursorEventos.moveToFirst();
                do{
                    long startMillis = 0;
                    long endMillis = 0;
                    long calID = 3;
                    Calendar beginTime = Calendar.getInstance();
                    beginTime.set(Integer.parseInt("20"+cursorEventos.getString(0).substring(1,3)), Integer.parseInt(cursorEventos.getString(0).substring(3,5)), Integer.parseInt(cursorEventos.getString(0).substring(5,7)));
                    startMillis = beginTime.getTimeInMillis();
                    Calendar endTime = Calendar.getInstance();
                    endTime.set(Integer.parseInt("20"+cursorEventos.getString(1).substring(1,3)), Integer.parseInt(cursorEventos.getString(1).substring(3,5)), Integer.parseInt(cursorEventos.getString(1).substring(5,7)));
                    endMillis = endTime.getTimeInMillis();
                    //calID= Long.parseLong(cursorEventos.getString(4));

                    ContentResolver cr = getContentResolver();
                    ContentValues values = new ContentValues();
                    values.put(CalendarContract.Events.DTSTART, startMillis);
                    values.put(CalendarContract.Events.DTEND, endMillis);
                    values.put(CalendarContract.Events.TITLE, cursorEventos.getString(2));
                    values.put(CalendarContract.Events.DESCRIPTION, cursorEventos.getString(3));
                    values.put(CalendarContract.Events.CALENDAR_ID, calID);
                    Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);
                }while(cursorEventos.moveToNext());
                cursorEventos.close();
            }

        } else if (id == R.id.nav_send) {
            SharedPreferences myPrefs = getSharedPreferences("LogeoUg", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = myPrefs.edit();
            editor.clear();
            editor.commit();
            //finish();
            Intent intent = new Intent (this, Login.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivityForResult(intent, 0);
            finish();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


    /*Thread hilo=new Thread(){
        public void run(){
            URL imageUrl = null;
            HttpURLConnection conn = null;

            try {
                imageUrl = new URL("https://2.bp.blogspot.com/_VS3Y8oEnTt4/TJEsfWARkpI/AAAAAAAAG4c/bvETfe4Yde0/w1200-h630-p-k-no-nu/adrian_mitchell_1238771c.jpg");
                conn = (HttpURLConnection) imageUrl.openConnection();
                conn.connect();
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 2; // el factor de escala a minimizar la imagen, siempre es potencia de 2
                Bitmap imagen = BitmapFactory.decodeStream(conn.getInputStream(), new Rect(0, 0, 0, 0), options);
                //imgparam.setImageBitmap(imagen);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };*/
    public class ImagenPortada extends AsyncTask<ImageView,Void,Void> {

        ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Cargando Imagen");
            pDialog.setCancelable(true);
            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(final ImageView... imageViews) {
/*
            final ImagesDownload hilo=new ImagesDownload();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    hilo.run(imageViews[0]);
                }
            }).start();*/
            URL imageUrl = null;
            HttpURLConnection conn = null;
            try {
                imageUrl = new URL("https://scontent-mia3-2.xx.fbcdn.net/v/t1.0-9/18486340_653638384830925_7547713726854800870_n.jpg?oh=c02a22308f543c8a71c3ea4d59c22119&oe=5A973265");
                conn = (HttpURLConnection) imageUrl.openConnection();
                conn.connect();
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 4; // el factor de escala a minimizar la imagen, siempre es potencia de 2
                Bitmap imagen = BitmapFactory.decodeStream(conn.getInputStream(), new Rect(0, 0, 0, 0), options);
                RoundedBitmapDrawable roundedBitmapDrawable= RoundedBitmapDrawableFactory.create(getResources(),imagen);
                roundedBitmapDrawable.setCornerRadius(imagen.getHeight());
                imageViews[0].setImageDrawable(roundedBitmapDrawable);
            } catch (IOException e) {
                e.printStackTrace();
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            pDialog.dismiss();
        }
    }


    static LocationManager locationManager;

    public static boolean isGPSProvider(Context context) {
        locationManager = (LocationManager) context.getSystemService(context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public static boolean isNetowrkProvider(Context context) {
        locationManager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

/*
    public static boolean isPassiveProvider(Context context) {
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER);
    }
*/
    public void ObtenerUbicacion() {

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        else{
            if(isNetowrkProvider(this)&&isGPSProvider(this)){
                Criteria criteria = new Criteria();
                criteria.setAccuracy(Criteria.ACCURACY_FINE);
                criteria.setAltitudeRequired(false);
                criteria.setBearingRequired(false);
                criteria.setCostAllowed(true);
                criteria.setPowerRequirement(Criteria.POWER_LOW);
                String provider = locationManager.getBestProvider(criteria, true);
                if (provider != null) {
                    locationManager.requestLocationUpdates(provider, 2 * 20 * 1000, 500, locationListenerBest);
                }
            }
            else if(isNetowrkProvider(this))
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 20 * 1000, 500, locationListenerNetwork);
            else if(isGPSProvider(this)) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2 * 20 * 1000, 500, locationListenerGPS);
            }
        }
    }

    private final LocationListener locationListenerBest = new LocationListener() {
        public void onLocationChanged(Location location) {

            SharedPreferences aux= PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
            if(aux.getBoolean("BanderaUbicacion", false)) {
                longitud = location.getLongitude();
                latitud = location.getLatitude();
                Toast.makeText(MainActivity.this, "LongitudBest:" + longitud + " Latitud:" + latitud, Toast.LENGTH_LONG).show();
            }
            else{
                locationManager.removeUpdates(locationListenerBest);
                locationManager = null;
            }
        }

        public void onStatusChanged(String s, int i, Bundle bundle) {
        }

        public void onProviderEnabled(String s) {
        }

        public void onProviderDisabled(String s) {
        }
    };

    private final LocationListener locationListenerNetwork = new LocationListener() {
        public void onLocationChanged(Location location) {
            SharedPreferences aux= PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
            if(aux.getBoolean("BanderaUbicacion", false)) {
                longitud = location.getLongitude();
                latitud = location.getLatitude();
                Toast.makeText(MainActivity.this, "LongitudNetwork:" + longitud + " Latitud:" + latitud, Toast.LENGTH_LONG).show();
            }
            else{
                locationManager.removeUpdates(locationListenerNetwork);
                locationManager = null;
            }
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };

    private final LocationListener locationListenerGPS = new LocationListener() {
        public void onLocationChanged(Location location) {

            SharedPreferences aux= PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
            if (aux.getBoolean("BanderaUbicacion", false)) {
                longitud = location.getLongitude();
                latitud = location.getLatitude();
                Toast.makeText(MainActivity.this, "LongitudGPS:" + longitud + " Latitud:" + latitud, Toast.LENGTH_LONG).show();
            } else {
                locationManager.removeUpdates(locationListenerGPS);
                locationManager = null;
            }
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
        }

        @Override
        public void onProviderEnabled(String s) {
        }

        @Override
        public void onProviderDisabled(String s) {
        }
    };
}