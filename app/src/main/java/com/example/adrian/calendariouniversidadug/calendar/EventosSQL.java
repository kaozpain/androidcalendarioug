package com.example.adrian.calendariouniversidadug.calendar;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by adrian on 02/12/17.
 */

public class EventosSQL extends SQLiteOpenHelper {


    String eventos = "CREATE TABLE ug_eventos (id_evento INTEGER, id_universidad INTEGER,id_campus INTEGER,id_facultad INTEGER," +
            "id_carrera INTEGER,id_materia INTEGER, fecha_inicio INTEGER, " +
            "fecha_fin INTEGER, titulo TEXT, descripcion TEXT, clases INTEGER)";
    String feriados = "CREATE TABLE ug_feriados (id_feriado INTEGER,id_ciudad INTEGER, fecha_inicio INTEGER, " +
            "fecha_fin INTEGER, clases INTEGER, titulo TEXT, descripcion TEXT)";
    String campus = "CREATE TABLE ug_campus (id_campus INTEGER,id_universidad INTEGER,nombre_campus TEXT, encargado INTEGER)";
    String carrera = "CREATE TABLE ug_carrera (id_carrera INTEGER,id_facultad INTEGER,nombre_carrera TEXT, encargado INTEGER)";
    String ciudad = "CREATE TABLE ug_ciudad (id_ciudad INTEGER,id_pais INTEGER,nombre TEXT)";
    String curso = "CREATE TABLE ug_curso (id_curso INTEGER,nombre TEXT,modalidad TEXT)";
    String facultad = "CREATE TABLE ug_facultad (id_facultad INTEGER,id_campus INTEGER,nombre_facultad TEXT, encargado INTEGER)";
    String materia = "CREATE TABLE ug_materia (id_materia INTEGER,cedula INTEGER,id_curso INTEGER," +
            "nombre TEXT,semestre INTEGER, id_carrera INTEGER)";
    String pais = "CREATE TABLE ug_pais (id_pais INTEGER,nombre TEXT)";
    String ubicacion_facultad = "CREATE TABLE ug_ubicacion_facultad (id_ubicacion INTEGER,id_facultad INTEGER," +
            "longitud REAL, latitud REAL)";
    String universidad = "CREATE TABLE ug_universidad (id_universidad INTEGER,id_ciudad INTEGER,nombre_universidad TEXT, encargado INTEGER)";
    String matricula = "CREATE TABLE ug_matricula (id_matricula INTEGER,cedula INTEGER,id_carrera INTEGER, fecha TEXT, fecha_exp TEXT" +
            ",id_materia1 INTEGER,id_materia2 INTEGER,id_materia3 INTEGER,id_materia4 INTEGER,id_materia5 INTEGER" +
            ",id_materia6 INTEGER,id_materia7 INTEGER,id_materia8 INTEGER,id_materia9 INTEGER)";





    public EventosSQL(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(eventos);
        db.execSQL(feriados);
        db.execSQL(campus);
        db.execSQL(carrera);
        db.execSQL(ciudad);
        db.execSQL(curso);
        db.execSQL(facultad);
        db.execSQL(materia);
        db.execSQL(pais);
        db.execSQL(ubicacion_facultad);
        db.execSQL(universidad);
        db.execSQL(matricula);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onDowngrade(db, oldVersion, newVersion);
        onUpgrade(db, oldVersion, newVersion);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS ug_eventos");
        db.execSQL("DROP TABLE IF EXISTS ug_feriados");
        db.execSQL("DROP TABLE IF EXISTS ug_campus");
        db.execSQL("DROP TABLE IF EXISTS ug_carrera");
        db.execSQL("DROP TABLE IF EXISTS ug_ciudad");
        db.execSQL("DROP TABLE IF EXISTS ug_curso");
        db.execSQL("DROP TABLE IF EXISTS ug_facultad");
        db.execSQL("DROP TABLE IF EXISTS ug_materia");
        db.execSQL("DROP TABLE IF EXISTS ug_pais");
        db.execSQL("DROP TABLE IF EXISTS ug_ubicacion_facultad");
        db.execSQL("DROP TABLE IF EXISTS ug_universidad");
        db.execSQL("DROP TABLE IF EXISTS ug_matricula");
        //Se crea la nueva versión de la tabla
        onCreate(db);
    }
}
