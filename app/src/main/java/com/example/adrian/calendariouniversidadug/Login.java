package com.example.adrian.calendariouniversidadug;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.adrian.calendariouniversidadug.calendar.Usuario;
import com.google.gson.Gson;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpResponseException;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

public class Login extends AppCompatActivity{

    private TextView lblGotoRegister;
    private Button btnLogin;
    private EditText inputUsuario;
    private EditText inputPassword;
    int permisoTotal = 220;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        inputUsuario = (EditText) findViewById(R.id.txtUsuario);
        inputPassword = (EditText) findViewById(R.id.txtPass);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        //loginErrorMsg = (TextView) findViewById(R.id.login_error);
        lblGotoRegister = (TextView) findViewById(R.id.link_to_register);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String usuario = inputUsuario.getText().toString().replace(" ", "");
                String password = inputPassword.getText().toString().replace(" ", "");
                if (usuario.equalsIgnoreCase("") || password.equalsIgnoreCase("")) {
                    if (usuario.equalsIgnoreCase("")) {
                        inputUsuario.setError("Campo no puede estar vacio");
                    }
                    if (password.equalsIgnoreCase("")) {
                        inputPassword.setError("Campo no puede estar vacio");
                    }
                } else {
                    operacionSoap aux=new operacionSoap();
                    aux.execute(usuario, password);
                    //aqui llamamos al controlador donde este el WS
                }
            }
        });

        lblGotoRegister.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                //x.ObtenerTelefono();
                Intent intent = new Intent(view.getContext(), Registro.class);
                startActivityForResult(intent, 0);
            }
        });


        if (ActivityCompat.checkSelfPermission(Login.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(Login.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(Login.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(Login.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(Login.this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(Login.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(Login.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(Login.this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(Login.this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA, android.Manifest.permission.READ_PHONE_STATE, android.Manifest.permission.SEND_SMS, android.Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET, Manifest.permission.READ_SMS},
                    permisoTotal);
        }

        //ObtenerUbicacion();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == permisoTotal) {

            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            } else {
                Toast.makeText(Login.this, permissions[0], Toast.LENGTH_LONG).show();
            }

            if (grantResults.length > 0
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

            } else {
                Toast.makeText(Login.this, permissions[1], Toast.LENGTH_LONG).show();
            }

            if (grantResults.length > 0
                    && grantResults[2] == PackageManager.PERMISSION_GRANTED) {

            } else {
                Toast.makeText(Login.this, permissions[2], Toast.LENGTH_LONG).show();
            }

            if (grantResults.length > 0
                    && grantResults[3] == PackageManager.PERMISSION_GRANTED) {

            } else {
                Toast.makeText(Login.this, permissions[3], Toast.LENGTH_LONG).show();
            }

            if (grantResults.length > 0
                    && grantResults[4] == PackageManager.PERMISSION_GRANTED) {

            } else {
                Toast.makeText(Login.this, permissions[4], Toast.LENGTH_LONG).show();
            }

            if (grantResults.length > 0
                    && grantResults[5] == PackageManager.PERMISSION_GRANTED) {

            } else {
                Toast.makeText(Login.this, permissions[5], Toast.LENGTH_LONG).show();
            }

            if (grantResults.length > 0
                    && grantResults[6] == PackageManager.PERMISSION_GRANTED) {

            } else {
                Toast.makeText(Login.this, permissions[6], Toast.LENGTH_LONG).show();
            }

            if (grantResults.length > 0
                    && grantResults[7] == PackageManager.PERMISSION_GRANTED) {

            } else {
                Toast.makeText(Login.this, permissions[7], Toast.LENGTH_LONG).show();
            }
            if (grantResults.length > 0
                    && grantResults[8] == PackageManager.PERMISSION_GRANTED) {

            } else {
                Toast.makeText(Login.this, permissions[8], Toast.LENGTH_LONG).show();
            }

            return;
        }
    }

    LocationManager locationManager;

    public void enviarMensaje(){
        TelephonyManager tMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        String numero=tMgr.getLine1Number();
        String operadora=tMgr.getSimOperatorName();
        String operadoraNetwork=tMgr.getNetworkOperatorName();

        String strPhone = numero;
        String strMessage = "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890TEST";

        if(operadora=="Claro"){
            strPhone="7887";
            strMessage=numero+" "+strMessage;
        }
        if(operadora=="Movistar"){
            strPhone="4858"+strPhone;
        }
        SmsManager sms = SmsManager.getDefault();

        ArrayList messageParts = sms.divideMessage(strMessage);

        sms.sendMultipartTextMessage(strPhone, null, messageParts, null, null);

        Toast.makeText(this, "Sent to"+numero+" operadora:"+operadora, Toast.LENGTH_SHORT).show();
    }

    /*public void ObtenerUbicacion() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2 * 20 * 1000, 10, locationListenerGPS);
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(true);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        String provider = locationManager.getBestProvider(criteria, true);
        if (provider != null) {
            locationManager.requestLocationUpdates(provider, 2 * 20 * 1000, 10, locationListenerBest);
        }

        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 20 * 1000, 10, locationListenerNetwork);
    }

    private final LocationListener locationListenerBest = new LocationListener() {
        public void onLocationChanged(Location location) {
            double longitudeBest = location.getLongitude();
            double latitudeBest = location.getLatitude();
            Toast.makeText(Login.this, "LongitudBest:"+longitudeBest+" Latitud:"+latitudeBest, Toast.LENGTH_LONG).show();
        }

        public void onStatusChanged(String s, int i, Bundle bundle) {
        }

        public void onProviderEnabled(String s) {
        }

        public void onProviderDisabled(String s) {
        }
    };

    private final LocationListener locationListenerNetwork = new LocationListener() {
        public void onLocationChanged(Location location) {
            Double longitudeNetwork = location.getLongitude();
            Double latitudeNetwork = location.getLatitude();
            Toast.makeText(Login.this, "LongitudNetwork:"+longitudeNetwork+" Latitud:"+latitudeNetwork, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };

    private final LocationListener locationListenerGPS = new LocationListener() {
        public void onLocationChanged(Location location) {
            Double longitudeGPS = location.getLongitude();
            Double latitudeGPS = location.getLatitude();
            Toast.makeText(Login.this, "LongitudGPS:"+longitudeGPS+" Latitud:"+latitudeGPS, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
        }

        @Override
        public void onProviderEnabled(String s) {
        }

        @Override
        public void onProviderDisabled(String s) {
        }
    };*/
    public class operacionSoap extends AsyncTask<String,String,String> {

        ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

            pDialog = new ProgressDialog(Login.this);
            pDialog.setMessage("Verificando datos");
            pDialog.setCancelable(false);
            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String strJson="33";
            final String NAMESPACE="http://tempuri.org/";
            final String METHODNAME="Autenticacion";
            //final String URL="http://192.168.1.4:8086/prueba1.asmx";
            //final String URL="http://192.168.1.130:8086/prueba1.asmx";
            final String URL=getString(R.string.ip);//casa de abuelita
            final String SOAPACTION=NAMESPACE+METHODNAME;
            SoapObject request=new SoapObject(NAMESPACE, METHODNAME);
            request.addProperty("usuario",params[0]);
            request.addProperty("clave",params[1]);
            SoapSerializationEnvelope envelope=new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet=true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE transporte=new HttpTransportSE(URL);
            Log.d("Transporte ", request.toString());
            try{
                transporte.call(SOAPACTION, envelope);
                //SoapObject response=(SoapObject) envelope.bodyIn;
                SoapPrimitive response=(SoapPrimitive) envelope.getResponse();
                strJson=response.toString();
                /*
                List<Usuario> recive=gson.fromJson(strJson,new TypeToken<List<Usuario>>(){}.getType());
                System.out.println();
                if( recive!= null ){
                    for(Usuario object : recive){
                        System.out.println("\nAlumno : " + object.getTipo() + " " + object.getEmail() );
                    }
                }*/
                //Toast.makeText(Registro.this,strJson,Toast.LENGTH_LONG).show();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (SoapFault soapFault) {
                soapFault.printStackTrace();
            } catch (HttpResponseException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e){
                e.printStackTrace();
            }
            return strJson;
        }

        @Override
        protected void onPostExecute(String usuarioaux) {
            super.onPostExecute(usuarioaux);
            //usuario="adrian272566@hotmail.es";
            //Toast.makeText(Registro.this,usuario,Toast.LENGTH_LONG).show();//usuario.cedula+" "+usuario.email+" "+usuario.tipo+" "+usuario.nombre,Toast.LENGTH_LONG).show();
            //mail.enviar(usuario.email);
            if(usuarioaux.equals("33"))Toast.makeText(Login.this, "Porfavor Intente mas tarde", Toast.LENGTH_SHORT).show();
            else{
                Gson gson = new Gson();
                Usuario recive = gson.fromJson(usuarioaux, Usuario.class);
                if(recive.getCodigo()==10){
                    Toast.makeText(Login.this, "Error al iniciar sesion, verifique sus datos", Toast.LENGTH_SHORT).show();
                } else if(recive.getCodigo()==33) {
                    Toast.makeText(Login.this, "Porfavor Intente mas tarde", Toast.LENGTH_SHORT).show();
                } else{
                    SharedPreferences auxLogin = getSharedPreferences("LogeoUg", MODE_PRIVATE);
                    SharedPreferences.Editor edita = auxLogin.edit();
                    edita.putString("nombre", recive.getNombre());
                    edita.putInt("cedula", recive.getCedula());
                    edita.putString("correo", recive.getCorreo());
                    edita.putInt("tipo", recive.getTipo() );
                    edita.commit();
                    finish();
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivityForResult(intent, 0);
                }
            }
            pDialog.dismiss();
        }
    }
}
