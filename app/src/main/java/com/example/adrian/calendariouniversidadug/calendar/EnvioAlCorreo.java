package com.example.adrian.calendariouniversidadug.calendar;

import android.os.StrictMode;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Created by adrian on 02/11/17.
 */

public class EnvioAlCorreo {
    public void enviar(String correo){
        Session sesion;
        Properties properties=new Properties();
        properties.put("mail.smtp.host","smtp.googlemail.com");
        properties.put("mail.smtp.socketFactory.port","465");
        properties.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
        properties.put("mail.smtp.auth","true");
        properties.put("mail.smtp.port","465");
        final emailApp aux=new emailApp();
        //BORRAR CUANDO TERMINE EL MODO DESARROLLO///////////////////////////////
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        /////////////////////////////////////////////////////////////////////////
        try {
            sesion=Session.getDefaultInstance(properties, new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(aux.getCorreo(), aux.getPass());
                }
            });

            if (sesion!=null){
                Message message=new MimeMessage(sesion);
                message.setFrom(new InternetAddress(aux.getCorreo()));
                message.setSubject(aux.getSubject());
                message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(correo));
                message.setContent(aux.getMensaje(),aux.getFormato());
                Transport.send(message);
            }
        }catch (Exception e){
            System.out.println(e);
        }

    }
}
