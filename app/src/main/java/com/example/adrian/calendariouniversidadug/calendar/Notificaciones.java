package com.example.adrian.calendariouniversidadug.calendar;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;

import com.example.adrian.calendariouniversidadug.MyFirebaseMessagingService;

/**
 * Created by adrian on 12/02/18.
 */

public class Notificaciones {
    public void enviar(Context context) {
        EventosSQL usdbh = new EventosSQL(context, "DBCalendar", null, 1);
        final SQLiteDatabase db = usdbh.getWritableDatabase();
        if (db != null) {
            final SharedPreferences auxiliar= PreferenceManager.getDefaultSharedPreferences(context);
            int tipoNoti=auxiliar.getInt("lista", 0);
            MyFirebaseMessagingService aux=new MyFirebaseMessagingService();
            String Mensaje= (tipoNoti==0)? "Usted tiene Eventos el dia de mañana":"Usted tiene Eventos el dia de hoy";
            String Mensaje2= (tipoNoti==0)? "Usted NO tiene Eventos el dia de mañana":"Usted NO tiene Eventos el dia de hoy";
            Cursor eventoNoti;
            if(tipoNoti==0)
                eventoNoti = db.rawQuery("select * from ug_universidad", null);//mañana
            else
                eventoNoti = db.rawQuery("select * from ug_universidad", null);//hoy
            if (eventoNoti != null) {
                aux.sendNotification(Mensaje, context);
            }
            else{
                aux.sendNotification(Mensaje2, context);
            }
        }
    }
}