package com.example.adrian.calendariouniversidadug.calendar;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.List;

/**
 * Created by adrian on 03/12/17.
 */

public class ConsumoActualizacion{

    //public static String urlServ="http://192.168.1.17:3000/prueba1.asmx";
    public static String urlServ="http://10.100.7.140:3000/prueba1.asmx";
    //public static String urlServ="http://10.100.1.166/prueba1.asmx";
    //public static String urlServ="http://192.168.0.21:3033/prueba1.asmx";

    public static class eventos extends AsyncTask<Integer,String,String>{
        public AsyncResponse delegate = null;

        public eventos(AsyncResponse delegate){
            this.delegate = delegate;
        }
        // Define interfaz.
        public interface AsyncResponse {
            void processFinish(List<Evento> output);
        }
        @Override
        protected String doInBackground(Integer... integers) {
            String strJson="33";
            final String NAMESPACE="http://tempuri.org/";
            final String METHODNAME="TodosLosEventos";
            final String URL=urlServ;
            //final String URL="http://192.168.1.130:8086/prueba1.asmx";
            //final String URL= "http://192.168.1.13:8086/prueba1.asmx";//casa de abuelita
            final String SOAPACTION=NAMESPACE+METHODNAME;
            SoapObject request=new SoapObject(NAMESPACE, METHODNAME);
            SoapSerializationEnvelope envelope=new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet=true;
            request.addProperty("id", integers[0]);
            envelope.setOutputSoapObject(request);
            HttpTransportSE transporte=new HttpTransportSE(URL);
            try {
                transporte.call(SOAPACTION, envelope);
                //SoapObject response=(SoapObject) envelope.bodyIn;
                SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
                strJson = response.toString();
            }catch (Exception e){

            }
            return strJson;
        }

        @Override
        protected void onPostExecute(String json) {
            super.onPostExecute(json);
            if(json.equals("33")){}
            else{
                Gson gson = new Gson();
                List<Evento> eventosList=null;
                eventosList = gson.fromJson(json, new TypeToken<List<Evento>>() {}.getType());
                delegate.processFinish(eventosList);
            }
        }
    }

    public static class feriados extends AsyncTask<Integer,String,String>{
        public AsyncResponse delegate = null;

        public feriados(AsyncResponse delegate){
            this.delegate = delegate;
        }
        // Define interfaz.
        public interface AsyncResponse {
            void processFinish(List<Feriado> output);
        }

        @Override
        protected String doInBackground(Integer... integers) {
            String strJson="33";
            final String NAMESPACE="http://tempuri.org/";
            final String METHODNAME="TodosLosFeriados";
            //final String URL="http://192.168.1.4:8086/prueba1.asmx";
            //final String URL="http://192.168.1.130:8086/prueba1.asmx";
            final String URL= urlServ;//casa de abuelita
            final String SOAPACTION=NAMESPACE+METHODNAME;
            SoapObject request=new SoapObject(NAMESPACE, METHODNAME);
            SoapSerializationEnvelope envelope=new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet=true;
            request.addProperty("id", integers[0]);
            envelope.setOutputSoapObject(request);
            HttpTransportSE transporte=new HttpTransportSE(URL);
            try {
                transporte.call(SOAPACTION, envelope);
                //SoapObject response=(SoapObject) envelope.bodyIn;
                SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
                strJson = response.toString();
            }catch (Exception e){

            }
            return strJson;
        }

        @Override
        protected void onPostExecute(String json) {
            super.onPostExecute(json);
            if(json.equals("33")){}
            else{
                Gson gson = new Gson();
                List<Feriado> feriadosList=null;
                feriadosList = gson.fromJson(json, new TypeToken<List<Feriado>>() {}.getType());
                delegate.processFinish(feriadosList);
            }
        }
    }

    public static class campus extends AsyncTask<Integer,String,String>{
        public AsyncResponse delegate = null;

        public campus(AsyncResponse delegate){
            this.delegate = delegate;
        }
        // Define interfaz.
        public interface AsyncResponse {
            void processFinish(List<Campus> output);
        }

        @Override
        protected String doInBackground(Integer... integers) {
            String strJson="33";
            final String NAMESPACE="http://tempuri.org/";
            final String METHODNAME="TodosLosCampus";
            //final String URL="http://192.168.1.4:8086/prueba1.asmx";
            //final String URL="http://192.168.1.130:8086/prueba1.asmx";
            final String URL= urlServ;//casa de abuelita
            final String SOAPACTION=NAMESPACE+METHODNAME;
            SoapObject request=new SoapObject(NAMESPACE, METHODNAME);
            SoapSerializationEnvelope envelope=new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet=true;
            request.addProperty("id", integers[0]);
            envelope.setOutputSoapObject(request);
            HttpTransportSE transporte=new HttpTransportSE(URL);
            try {
                transporte.call(SOAPACTION, envelope);
                //SoapObject response=(SoapObject) envelope.bodyIn;
                SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
                strJson = response.toString();
            }catch (Exception e){

            }
            return strJson;
        }

        @Override
        protected void onPostExecute(String json) {
            super.onPostExecute(json);
            if(json.equals("33")){}
            else{
                Gson gson = new Gson();
                List<Campus> campusesList=null;
                campusesList = gson.fromJson(json, new TypeToken<List<Campus>>() {}.getType());
                delegate.processFinish(campusesList);
            }
        }
    }

    public static class carrera extends AsyncTask<Integer,String,String>{
        public AsyncResponse delegate = null;

        public carrera(AsyncResponse delegate){
            this.delegate = delegate;
        }
        // Define interfaz.
        public interface AsyncResponse {
            void processFinish(List<Carrera> output);
        }
        @Override
        protected String doInBackground(Integer... integers) {
            String strJson="33";
            final String NAMESPACE="http://tempuri.org/";
            final String METHODNAME="TodasLasCarreras";
            //final String URL="http://192.168.1.4:8086/prueba1.asmx";
            //final String URL="http://192.168.1.130:8086/prueba1.asmx";
            final String URL= urlServ;//casa de abuelita
            final String SOAPACTION=NAMESPACE+METHODNAME;
            SoapObject request=new SoapObject(NAMESPACE, METHODNAME);
            SoapSerializationEnvelope envelope=new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet=true;
            request.addProperty("id", integers[0]);
            envelope.setOutputSoapObject(request);
            HttpTransportSE transporte=new HttpTransportSE(URL);
            try {
                transporte.call(SOAPACTION, envelope);
                //SoapObject response=(SoapObject) envelope.bodyIn;
                SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
                strJson = response.toString();
            }catch (Exception e){

            }
            return strJson;
        }

        @Override
        protected void onPostExecute(String json) {
            super.onPostExecute(json);
            if(json.equals("33")){}
            else{
                Gson gson = new Gson();
                List<Carrera> carrerasList=null;
                carrerasList = gson.fromJson(json, new TypeToken<List<Carrera>>() {}.getType());
                delegate.processFinish(carrerasList);
            }
        }
    }


    public static class ciudades extends AsyncTask<Integer,String,String>{
        public AsyncResponse delegate = null;

        public ciudades(AsyncResponse delegate){
            this.delegate = delegate;
        }
        // Define interfaz.
        public interface AsyncResponse {
            void processFinish(List<Ciudad> output);
        }

        @Override
        protected String doInBackground(Integer... integers) {
            String strJson="33";
            final String NAMESPACE="http://tempuri.org/";
            final String METHODNAME="TodasLasCiudades";
            //final String URL="http://192.168.1.4:8086/prueba1.asmx";
            //final String URL="http://192.168.1.130:8086/prueba1.asmx";
            final String URL= urlServ;//casa de abuelita
            final String SOAPACTION=NAMESPACE+METHODNAME;
            SoapObject request=new SoapObject(NAMESPACE, METHODNAME);
            SoapSerializationEnvelope envelope=new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet=true;
            request.addProperty("id", integers[0]);
            envelope.setOutputSoapObject(request);
            HttpTransportSE transporte=new HttpTransportSE(URL);
            try {
                transporte.call(SOAPACTION, envelope);
                //SoapObject response=(SoapObject) envelope.bodyIn;
                SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
                strJson = response.toString();
            }catch (Exception e){

            }
            return strJson;
        }

        @Override
        protected void onPostExecute(String json) {
            super.onPostExecute(json);
            if(json.equals("33")){}
            else{
                Gson gson = new Gson();
                List<Ciudad> ciudadesList=null;
                ciudadesList = gson.fromJson(json, new TypeToken<List<Ciudad>>() {}.getType());
                delegate.processFinish(ciudadesList);
            }
        }
    }

    public static class cursos extends AsyncTask<Integer,String,String>{
        public AsyncResponse delegate = null;

        public cursos(AsyncResponse delegate){
            this.delegate = delegate;
        }
        // Define interfaz.
        public interface AsyncResponse {
            void processFinish(List<Cursos> output);
        }

        @Override
        protected String doInBackground(Integer... integers) {
            String strJson="33";
            final String NAMESPACE="http://tempuri.org/";
            final String METHODNAME="TodosLosCursos";
            //final String URL="http://192.168.1.4:8086/prueba1.asmx";
            //final String URL="http://192.168.1.130:8086/prueba1.asmx";
            final String URL= urlServ;//casa de abuelita
            final String SOAPACTION=NAMESPACE+METHODNAME;
            SoapObject request=new SoapObject(NAMESPACE, METHODNAME);
            SoapSerializationEnvelope envelope=new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet=true;
            request.addProperty("id", integers[0]);
            envelope.setOutputSoapObject(request);
            HttpTransportSE transporte=new HttpTransportSE(URL);
            try {
                transporte.call(SOAPACTION, envelope);
                //SoapObject response=(SoapObject) envelope.bodyIn;
                SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
                strJson = response.toString();
            }catch (Exception e){

            }

            return strJson;
        }

        @Override
        protected void onPostExecute(String json) {
            super.onPostExecute(json);
            if(json.equals("33")){}
            else{
                Gson gson = new Gson();
                List<Cursos> cursosList=null;
                cursosList = gson.fromJson(json, new TypeToken<List<Cursos>>() {}.getType());
                delegate.processFinish(cursosList);
            }
        }
    }


    public static class facultad extends AsyncTask<Integer,String,String>{
        public AsyncResponse delegate = null;

        public facultad(AsyncResponse delegate){
            this.delegate = delegate;
        }
        // Define interfaz.
        public interface AsyncResponse {
            void processFinish(List<Facultad> output);
        }

        @Override
        protected String doInBackground(Integer... integers) {
            String strJson="33";
            final String NAMESPACE="http://tempuri.org/";
            final String METHODNAME="TodasLasFacultades";
            //final String URL="http://192.168.1.4:8086/prueba1.asmx";
            //final String URL="http://192.168.1.130:8086/prueba1.asmx";
            final String URL= urlServ;//casa de abuelita
            final String SOAPACTION=NAMESPACE+METHODNAME;
            SoapObject request=new SoapObject(NAMESPACE, METHODNAME);
            SoapSerializationEnvelope envelope=new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet=true;
            request.addProperty("id", integers[0]);
            envelope.setOutputSoapObject(request);
            HttpTransportSE transporte=new HttpTransportSE(URL);
            try {
                transporte.call(SOAPACTION, envelope);
                //SoapObject response=(SoapObject) envelope.bodyIn;
                SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
                strJson = response.toString();
            }catch (Exception e){

            }
            return strJson;
        }

        @Override
        protected void onPostExecute(String json) {
            super.onPostExecute(json);
            if(json.equals("33")){}
            else{
                Gson gson = new Gson();
                List<Facultad> facultadesList=null;
                facultadesList = gson.fromJson(json, new TypeToken<List<Facultad>>() {}.getType());
                delegate.processFinish(facultadesList);
            }
        }
    }


    public static class materia extends AsyncTask<Integer,String,String>{
        public AsyncResponse delegate = null;

        public materia(AsyncResponse delegate){
            this.delegate = delegate;
        }
        // Define interfaz.
        public interface AsyncResponse {
            void processFinish(List<Materia> output);
        }

        @Override
        protected String doInBackground(Integer... integers) {
            String strJson="33";
            final String NAMESPACE="http://tempuri.org/";
            final String METHODNAME="TodasLasMaterias";
            //final String URL="http://192.168.1.4:8086/prueba1.asmx";
            //final String URL="http://192.168.1.130:8086/prueba1.asmx";
            final String URL= urlServ;//casa de abuelita
            final String SOAPACTION=NAMESPACE+METHODNAME;
            SoapObject request=new SoapObject(NAMESPACE, METHODNAME);
            SoapSerializationEnvelope envelope=new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet=true;
            request.addProperty("id", integers[0]);
            envelope.setOutputSoapObject(request);
            HttpTransportSE transporte=new HttpTransportSE(URL);
            try {
                transporte.call(SOAPACTION, envelope);
                //SoapObject response=(SoapObject) envelope.bodyIn;
                SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
                strJson = response.toString();
            }catch (Exception e){

            }
            return strJson;
        }

        @Override
        protected void onPostExecute(String json) {
            super.onPostExecute(json);
            if(json.equals("33")){}
            else{
                Gson gson = new Gson();
                List<Materia> materiasList=null;
                materiasList = gson.fromJson(json, new TypeToken<List<Materia>>() {}.getType());
                delegate.processFinish(materiasList);
            }
        }
    }

    public static class pais extends AsyncTask<Integer,String,String>{
        public AsyncResponse delegate = null;

        public pais(AsyncResponse delegate){
            this.delegate = delegate;
        }
        // Define interfaz.
        public interface AsyncResponse {
            void processFinish(List<Pais> output);
        }

        @Override
        protected String doInBackground(Integer... integers) {
            String strJson="33";
            final String NAMESPACE="http://tempuri.org/";
            final String METHODNAME="TodosLosPaises";
            //final String URL="http://192.168.1.4:8086/prueba1.asmx";
            //final String URL="http://192.168.1.130:8086/prueba1.asmx";
            final String URL= urlServ;//casa de abuelita
            final String SOAPACTION=NAMESPACE+METHODNAME;
            SoapObject request=new SoapObject(NAMESPACE, METHODNAME);
            SoapSerializationEnvelope envelope=new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet=true;
            request.addProperty("id", integers[0]);
            envelope.setOutputSoapObject(request);
            HttpTransportSE transporte=new HttpTransportSE(URL);
            try {
                transporte.call(SOAPACTION, envelope);
                //SoapObject response=(SoapObject) envelope.bodyIn;
                SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
                strJson = response.toString();
            }catch (Exception e){

            }

            return strJson;
        }

        @Override
        protected void onPostExecute(String json) {
            super.onPostExecute(json);
            if(json.equals("33")){}
            else{
                Gson gson = new Gson();
                List<Pais> paisesList=null;
                paisesList = gson.fromJson(json, new TypeToken<List<Pais>>() {}.getType());
                delegate.processFinish(paisesList);
            }
        }
    }


    public static class ubicacion_facultad extends AsyncTask<Integer,String,String>{
        public AsyncResponse delegate = null;

        public ubicacion_facultad(AsyncResponse delegate){
            this.delegate = delegate;
        }
        // Define interfaz.
        public interface AsyncResponse {
            void processFinish(List<Ubicacion> output);
        }

        @Override
        protected String doInBackground(Integer... integers) {
            String strJson="33";
            final String NAMESPACE="http://tempuri.org/";
            final String METHODNAME="TodasLasUbicaciones";
            //final String URL="http://192.168.1.4:8086/prueba1.asmx";
            //final String URL="http://192.168.1.130:8086/prueba1.asmx";
            final String URL= urlServ;//casa de abuelita
            final String SOAPACTION=NAMESPACE+METHODNAME;
            SoapObject request=new SoapObject(NAMESPACE, METHODNAME);
            SoapSerializationEnvelope envelope=new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet=true;
            request.addProperty("id", integers[0]);
            envelope.setOutputSoapObject(request);
            HttpTransportSE transporte=new HttpTransportSE(URL);
            try {
                transporte.call(SOAPACTION, envelope);
                //SoapObject response=(SoapObject) envelope.bodyIn;
                SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
                strJson = response.toString();
            }catch (Exception e){

            }
            return strJson;
        }

        @Override
        protected void onPostExecute(String json) {
            super.onPostExecute(json);
            if(json.equals("33")){}
            else{
                Gson gson = new Gson();
                List<Ubicacion> ubicacionesList=null;
                ubicacionesList = gson.fromJson(json, new TypeToken<List<Ubicacion>>() {}.getType());
                delegate.processFinish(ubicacionesList);
            }
        }
    }


    public static class universidad extends AsyncTask<Integer,String,String>{
        public AsyncResponse delegate = null;

        public universidad(AsyncResponse delegate){
            this.delegate = delegate;
        }
        // Define interfaz.
        public interface AsyncResponse {
            void processFinish(List<Universidad> output);
        }

        @Override
        protected String doInBackground(Integer... integers) {
            String strJson="33";
            final String NAMESPACE="http://tempuri.org/";
            final String METHODNAME="TodasLasUniversidades";
            //final String URL="http://192.168.1.4:8086/prueba1.asmx";
            //final String URL="http://192.168.1.130:8086/prueba1.asmx";
            final String URL= urlServ;//casa de abuelita
            final String SOAPACTION=NAMESPACE+METHODNAME;
            SoapObject request=new SoapObject(NAMESPACE, METHODNAME);
            SoapSerializationEnvelope envelope=new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet=true;
            request.addProperty("id", integers[0]);
            envelope.setOutputSoapObject(request);
            HttpTransportSE transporte=new HttpTransportSE(URL);
            try {
                transporte.call(SOAPACTION, envelope);
                //SoapObject response=(SoapObject) envelope.bodyIn;
                SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
                strJson = response.toString();
            }catch (Exception e){

            }
            return strJson;
        }

        @Override
        protected void onPostExecute(String json) {
            super.onPostExecute(json);
            if(json.equals("33")){}
            else{
                Gson gson = new Gson();
                List<Universidad> universidadesList=null;
                universidadesList = gson.fromJson(json, new TypeToken<List<Universidad>>() {}.getType());
                delegate.processFinish(universidadesList);
            }
        }
    }

    public static class matricula extends AsyncTask<Integer,String,String>{
        public AsyncResponse delegate = null;

        public matricula(AsyncResponse delegate){
            this.delegate = delegate;
        }
        // Define interfaz.
        public interface AsyncResponse {
            void processFinish(Matricula output);
        }

        @Override
        protected String doInBackground(Integer... integers) {
            String strJson="33";
            final String NAMESPACE="http://tempuri.org/";
            final String METHODNAME="MatriculaVigente";
            final String URL= urlServ;//casa de abuelita
            final String SOAPACTION=NAMESPACE+METHODNAME;
            SoapObject request=new SoapObject(NAMESPACE, METHODNAME);
            SoapSerializationEnvelope envelope=new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet=true;
            request.addProperty("cedula", integers[0]);
            request.addProperty("id", integers[1]);
            envelope.setOutputSoapObject(request);
            HttpTransportSE transporte=new HttpTransportSE(URL);
            try {
                transporte.call(SOAPACTION, envelope);
                //SoapObject response=(SoapObject) envelope.bodyIn;
                SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
                strJson = response.toString();
            }catch (Exception e){
                Log.i("error",""+e);
            }
            return strJson;
        }

        @Override
        protected void onPostExecute(String json) {
            super.onPostExecute(json);
            if(json.equals("33")){}
            else{
                if(json.equals("10")){/*Matricula ya actualizada o alumno no matriculado*/}
                else{
                    Gson gson = new Gson();
                    Matricula matricula=null;
                    matricula = gson.fromJson(json, Matricula.class);
                    delegate.processFinish(matricula);
                }
            }
        }
    }
}
