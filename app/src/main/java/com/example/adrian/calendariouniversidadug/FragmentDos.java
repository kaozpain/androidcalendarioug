package com.example.adrian.calendariouniversidadug;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.adrian.calendariouniversidadug.calendar.Evento;
import com.example.adrian.calendariouniversidadug.calendar.EventosSQL;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentDos.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentDos#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentDos extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public List<Evento> recive=null;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentDos() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentDos.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentDos newInstance(String param1, String param2) {
        FragmentDos fragment = new FragmentDos();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, LinkedList<Evento>> listDataChild;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_fragment_dos, container, false);
        int dia= getArguments().getInt("dia", 0);
        int mes= getArguments().getInt("mes", 0)+1;
        int ano= getArguments().getInt("ano", 0);
        String diaString=(dia<10)? "0"+dia:""+dia;
        String mesString=(mes<10)? "0"+mes:""+mes;
        /*if(dia<10)
            diaString="0"+dia;
        else diaString=""+dia;

        if(mes<10)
            mesString="0"+mes;
        else mesString=""+mes;*/
        ano=ano%100;
        final String diaSeleccionado="1"+ano+mesString+diaString;
        /*
        Date date=new Date(diaSeleccionado);
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd/MM/yy");
        try {
            diaSeleccionado= simpleDateFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        SharedPreferences auxLogin= this.getActivity().getSharedPreferences("LogeoUg", Context.MODE_PRIVATE);
        Integer accesoTipo=auxLogin.getInt("tipo",0);
        Integer accesoCedula=auxLogin.getInt("cedula",0);

        expListView=(ExpandableListView) v.findViewById(R.id.MenuExpandible);

        operacionSoap WSllamada=new operacionSoap();
        WSllamada.execute(diaSeleccionado, accesoTipo.toString(), accesoCedula.toString());
        /*listAdapter = new ExpandableListAdapter(v.getContext(), listDataHeader, listDataChild);
        // setting list adapter
        expListView.setAdapter(listAdapter);*/
        TextView fecha=(TextView) v.findViewById(R.id.fecha);
        fecha.setText(diaSeleccionado);
        FloatingActionButton fab = (FloatingActionButton) v.findViewById(R.id.fab);

        Calendar hoy=Calendar.getInstance();
        Calendar DiaEscogido = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy");
        Date date = new Date();
        Date date2 = new Date();
        String diaDeHoy=formatter.format(date);
        try {
            date=formatter.parse(dia+"/"+mes+"/"+ano);
            date2=formatter.parse(diaDeHoy);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DiaEscogido.setTime(date);
        hoy.setTime(date2);

        if(accesoTipo>=5||DiaEscogido.getTimeInMillis()<hoy.getTimeInMillis()){
            fab.setVisibility(v.GONE);
        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarEvento x = new AgregarEvento();
                Bundle bundle = new Bundle();
                bundle.putString("DiaSeleccionado",diaSeleccionado);
                x.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.Contendedor, x).addToBackStack(null).commit();
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
            }
        });
        //ListView list=(ListView) getActivity().findViewById(R.id.listaEventos);

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public class operacionSoap extends AsyncTask<String,String,String> {

        ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

            pDialog = new ProgressDialog(getContext());
            pDialog.setMessage("Obteniendo datos");
            pDialog.setCancelable(false);
            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String result="33";
            try{
                //String[] fechaSeleccionada=params[0].split("/");
                int fechaSeleccionada=Integer.parseInt(params[0]);
                EventosSQL usdbh = new EventosSQL(getContext(), "DBCalendar", null, 1);
                final SQLiteDatabase db = usdbh.getWritableDatabase();
                //Cursor cursor = db.rawQuery("select * from ug_eventos where "+format.parse(params[0])+" between "+ format.parse("fecha_inicio")+" and "+ format.parse("fecha_fin"), null);
                SharedPreferences aux= PreferenceManager.getDefaultSharedPreferences(getActivity());
                Integer accesoTipo=Integer.parseInt(params[1]);
                Integer accesoCedula=Integer.parseInt(params[2]);
                String sqlWhere="";
                if(accesoTipo==1){//rector
                    String idUniversidad="";
                    List<String> idCampus=new ArrayList<String>();
                    Cursor cursorRectorUniversidad = db.rawQuery("select id_universidad from ug_universidad where encargado=" + accesoCedula, null);//aqui se debe aumentar logica
                    if (cursorRectorUniversidad != null) {
                        cursorRectorUniversidad.moveToFirst();
                        idUniversidad=cursorRectorUniversidad.getString(0);
                        cursorRectorUniversidad.close();
                    }
                    sqlWhere= aux.getBoolean("Universidad", true)? sqlWhere+"id_universidad="+idUniversidad+" or " : sqlWhere+"";

                    Cursor cursorRectorCampus = db.rawQuery("select id_campus from ug_campus where id_universidad=" + idUniversidad, null);//aqui se debe aumentar logica
                    if (cursorRectorCampus != null) {
                        cursorRectorCampus.moveToFirst();
                        do {
                            idCampus.add(cursorRectorCampus.getString(0));
                            sqlWhere =aux.getBoolean("Campus", true)? sqlWhere + " id_campus=" + cursorRectorCampus.getString(0) + " or ":sqlWhere+"";
                        } while (cursorRectorCampus.moveToNext());
                        cursorRectorCampus.close();
                    }

                    if(aux.getBoolean("Facultad", true)){
                        String where="";
                        for (String auxiliar: idCampus) {
                            where=where+"id_campus="+auxiliar+" or ";
                        }
                        where=where.substring(0, where.length()-3);
                        Cursor cursorRectorFacultad = db.rawQuery("select id_facultad from ug_facultad where "+ where, null);//aqui se debe aumentar logica
                        if (cursorRectorFacultad != null) {
                            cursorRectorFacultad.moveToFirst();
                            do {
                                sqlWhere =aux.getBoolean("Facultad", true)? sqlWhere + " id_facultad=" + cursorRectorFacultad.getString(0) + " or ":sqlWhere+"";
                            } while (cursorRectorFacultad.moveToNext());
                            cursorRectorFacultad.close();
                        }
                    }
                    else if(aux.getBoolean("BanderaUbicacion", false)){
                        Double longitud= MainActivity.longitud;
                        Double latitud=MainActivity.latitud;
                        Cursor cursorRectorFacultad = db.rawQuery("select id_facultad from ug_ubicacion_facultad where" +
                                " longitud>="+(MainActivity.longitud-1)+ " and longitud<="+(MainActivity.longitud+1)+" and " +
                                "latitud>="+(MainActivity.latitud-1)+ " and latitud<="+(MainActivity.latitud+1), null);//aqui se debe aumentar logica
                        if (cursorRectorFacultad != null) {
                            cursorRectorFacultad.moveToFirst();
                            do {
                                sqlWhere =sqlWhere + " id_facultad=" + cursorRectorFacultad.getString(0) + " or ";
                            } while (cursorRectorFacultad.moveToNext());
                            cursorRectorFacultad.close();
                        }
                    }
                }
                else if(accesoTipo==2){//decano
                    String idFacultad="";
                    String idCampus="";
                    Cursor cursorDecanoFacultad = db.rawQuery("select id_facultad, id_campus from ug_facultad where encargado=" + accesoCedula, null);//aqui se debe aumentar logica
                    if (cursorDecanoFacultad != null) {
                        cursorDecanoFacultad.moveToFirst();
                        idFacultad=cursorDecanoFacultad.getString(0);
                        idCampus=cursorDecanoFacultad.getString(1);
                        cursorDecanoFacultad.close();
                    }
                    sqlWhere= aux.getBoolean("Facultad", true)? sqlWhere+"id_facultad="+idFacultad+" or " : sqlWhere+"";
                    sqlWhere=aux.getBoolean("Campus", true)? sqlWhere+"id_campus="+idCampus+" or " : sqlWhere+"";

                    Cursor cursorDecanoUniversidad = db.rawQuery("select id_universidad from ug_campus where id_campus=" + idCampus, null);//aqui se debe aumentar logica
                    if (cursorDecanoUniversidad != null) {
                        cursorDecanoUniversidad.moveToFirst();
                        sqlWhere= aux.getBoolean("Universidad", true)? sqlWhere+"id_universidad="+cursorDecanoUniversidad.getString(0)+" or " : sqlWhere+"";
                        cursorDecanoUniversidad.close();
                    }

                    if(aux.getBoolean("Carrera", true)){
                        Cursor cursorDecanoCarrera = db.rawQuery("select id_carrera from ug_carrera where id_facultad=" + idFacultad, null);//aqui se debe aumentar logica
                        if (cursorDecanoCarrera != null) {
                            cursorDecanoCarrera.moveToFirst();
                            do{
                                sqlWhere=sqlWhere+"id_carrera="+cursorDecanoCarrera.getString(0)+" or ";
                            } while (cursorDecanoCarrera.moveToNext());
                            cursorDecanoCarrera.close();
                        }
                    }
                }
                else if(accesoTipo==3){//director
                    String idCarrera="";
                    String idFacultad="";
                    String idCampus="";
                    Cursor cursorDirectorCarrera = db.rawQuery("select id_carrera, id_facultad from ug_carrera where encargado=" + accesoCedula, null);//aqui se debe aumentar logica
                    if (cursorDirectorCarrera != null) {
                        cursorDirectorCarrera.moveToFirst();
                        idCarrera=cursorDirectorCarrera.getString(0);
                        idFacultad=cursorDirectorCarrera.getString(1);
                        cursorDirectorCarrera.close();
                    }
                    sqlWhere= aux.getBoolean("Carrera", true)? sqlWhere+"id_carrera="+idCarrera+" or " : sqlWhere+"";
                    sqlWhere= aux.getBoolean("Facultad", true)? sqlWhere+"id_facultad="+idFacultad+" or " : sqlWhere+"";

                    Cursor cursorDirectorFacultad = db.rawQuery("select id_campus from ug_facultad where id_facultad=" + idFacultad, null);//aqui se debe aumentar logica
                    if (cursorDirectorFacultad != null) {
                        cursorDirectorFacultad.moveToFirst();
                        idCampus=cursorDirectorFacultad.getString(0);
                        cursorDirectorFacultad.close();
                    }
                    sqlWhere=aux.getBoolean("Campus", true)? sqlWhere+"id_campus="+idCampus+" or " : sqlWhere+"";

                    if(aux.getBoolean("Universidad", true)){
                        Cursor cursorDirectorUniversidad = db.rawQuery("select id_universidad from ug_campus where id_campus=" + idCampus, null);//aqui se debe aumentar logica
                        if (cursorDirectorUniversidad != null) {
                            cursorDirectorUniversidad.moveToFirst();
                            sqlWhere= sqlWhere+"id_universidad="+cursorDirectorUniversidad.getString(0)+" or ";
                            cursorDirectorUniversidad.close();
                        }
                    }
                }
                else if(accesoTipo==4){//profesor
                    List<String> carreraList=new ArrayList<String>();
                    String idFacultad="";
                    String idCampus="";
                    Cursor cursorProfesorMateria = db.rawQuery("select id_materia, id_carrera from ug_materia where cedula=" + accesoCedula, null);//aqui se debe aumentar logica
                    if (cursorProfesorMateria != null) {
                        cursorProfesorMateria.moveToFirst();
                        do {
                            if(!carreraList.contains(cursorProfesorMateria.getString(1))){
                                carreraList.add(cursorProfesorMateria.getString(1));
                                sqlWhere = aux.getBoolean("Carrera", true)? sqlWhere + " id_carrera=" + cursorProfesorMateria.getString(1) + " or ":sqlWhere+"";
                            }
                            sqlWhere = aux.getBoolean("Materias", true)? sqlWhere + " id_materia=" + cursorProfesorMateria.getString(0) + " or ":sqlWhere+"";
                        } while (cursorProfesorMateria.moveToNext());
                        cursorProfesorMateria.close();
                    }

                    //solo sirve si el profesor esta en una facultad
                    Cursor cursorProfesorCarrera = db.rawQuery("select id_facultad from ug_carrera where id_carrera=" + carreraList.get(0), null);//aqui se debe aumentar logica
                    if (cursorProfesorCarrera != null) {
                        cursorProfesorCarrera.moveToFirst();
                        idFacultad=cursorProfesorCarrera.getString(0);
                        cursorProfesorCarrera.close();
                    }
                    sqlWhere = aux.getBoolean("Facultad", true)? sqlWhere + " id_facultad=" + idFacultad + " or ":sqlWhere+"";


                    Cursor cursorProfesorFacultad = db.rawQuery("select id_campus from ug_facultad where id_facultad=" + idFacultad, null);//aqui se debe aumentar logica
                    if (cursorProfesorFacultad != null) {
                        cursorProfesorFacultad.moveToFirst();
                        idCampus=cursorProfesorFacultad.getString(0);
                        cursorProfesorFacultad.close();
                    }
                    sqlWhere=aux.getBoolean("Campus", true)? sqlWhere+"id_campus="+idCampus+" or " : sqlWhere+"";

                    if(aux.getBoolean("Universidad", true)) {
                        Cursor cursorProfesorCampus = db.rawQuery("select id_universidad from ug_campus where id_campus=" + idCampus, null);//aqui se debe aumentar logica
                        if (cursorProfesorCampus != null) {
                            cursorProfesorCampus.moveToFirst();
                            sqlWhere = sqlWhere + "id_universidad=" + cursorProfesorCampus.getString(0) + " or ";
                            cursorProfesorCampus.close();
                        }
                    }
                }

                else if(accesoTipo==5){//estudiante
                    String idCarrera="";
                    String idFacultad="";
                    String idCampus="";
                    Cursor cursorAlumno = db.rawQuery("select id_carrera, id_materia1, id_materia2, id_materia3, id_materia4" +
                            ", id_materia5, id_materia6, id_materia7, id_materia8, id_materia9 from ug_matricula", null);//aqui se debe aumentar logica
                    if (cursorAlumno != null) {
                        cursorAlumno.moveToFirst();
                        idCarrera=cursorAlumno.getString(0);
                        if(aux.getBoolean("Materias", true)) {
                        int totalColumna = cursorAlumno.getColumnCount();
                        for (int i = 1; i < totalColumna; i++) {
                            if (cursorAlumno.getColumnName(i) != null) {
                                try {
                                    if (!(cursorAlumno.getString(i) == null || cursorAlumno.getString(i).isEmpty() || cursorAlumno.getString(i) == "" || cursorAlumno.getInt(i) == 0))
                                        sqlWhere = sqlWhere + " id_materia=" + cursorAlumno.getString(i) + " or ";
                                    } catch (Exception e) {
                                        Log.d("TAG_NAME", e.getMessage());
                                    }
                                }
                            }
                        }
                        cursorAlumno.close();
                    }
                    sqlWhere= aux.getBoolean("Carrera", true)? sqlWhere+"id_carrera="+idCarrera+" or " : "";

                    Cursor cursorAlumnoCarrera = db.rawQuery("select id_facultad from ug_carrera where id_carrera=" + idCarrera, null);//aqui se debe aumentar logica
                    if (cursorAlumnoCarrera != null) {
                        cursorAlumnoCarrera.moveToFirst();
                        idFacultad=cursorAlumnoCarrera.getString(0);
                        cursorAlumnoCarrera.close();
                    }
                    sqlWhere = aux.getBoolean("Facultad", true) ? sqlWhere + "id_facultad=" + idFacultad + " or " : sqlWhere + "";

                    Cursor cursorAlumnoFacultad = db.rawQuery("select id_campus from ug_facultad where id_facultad=" + idFacultad, null);//aqui se debe aumentar logica
                    if (cursorAlumnoFacultad != null) {
                        cursorAlumnoFacultad.moveToFirst();
                        idCampus=cursorAlumnoFacultad.getString(0);
                        cursorAlumnoFacultad.close();
                    }
                    sqlWhere = aux.getBoolean("Campus", true) ? sqlWhere + "id_campus=" + idCampus + " or " : sqlWhere + "";

                    if(aux.getBoolean("Universidad", true)){
                        Cursor cursorAlumnoCampus = db.rawQuery("select id_universidad from ug_campus where id_campus=" + idCampus, null);//aqui se debe aumentar logica
                        if (cursorAlumnoCampus != null) {
                            cursorAlumnoCampus.moveToFirst();
                            sqlWhere = sqlWhere + "id_universidad=" + cursorAlumnoCampus.getString(0) + " or ";
                            cursorAlumnoCampus.close();
                        }
                    }
                }

                if(!(sqlWhere==""||sqlWhere.isEmpty())) {
                    sqlWhere = sqlWhere.substring(0, sqlWhere.length() - 3);
                    sqlWhere = " and (" + sqlWhere + ")";
                }

                Cursor cursor = db.rawQuery("select * from ug_eventos where (fecha_inicio<="+fechaSeleccionada+" and fecha_fin>="+fechaSeleccionada+")"+sqlWhere, null);//aqui se debe aumentar logica
                //List<Evento> eventosAux=null;
                JSONArray resultSet= new JSONArray();
                if (cursor != null) {
                    cursor.moveToFirst();
                    do{
                        int totalColumn = cursor.getColumnCount();
                        JSONObject rowObject = new JSONObject();
                        for( int i=0 ;  i< totalColumn ; i++ )
                        {
                            if( cursor.getColumnName(i) != null )
                            {
                                try
                                {
                                    if( cursor.getString(i) != null )
                                    {
                                        rowObject.put(cursor.getColumnName(i) ,  cursor.getString(i) );
                                    }
                                    else
                                    {
                                        rowObject.put( cursor.getColumnName(i) ,  "" );
                                    }
                                }
                                catch( Exception e )
                                {
                                    Log.d("TAG_NAME", e.getMessage()  );
                                }
                            }
                        }
                        resultSet.put(rowObject);
                    }while (cursor.moveToNext());
                    result=resultSet.toString();
                    cursor.close();
                }
            }catch (Exception e){
                Log.d("TAG_NAME", "error"+e);
                result="12";
            }
            return result;
        }

        @Override
        protected void onPostExecute(String json) {
            super.onPostExecute(json);
            //final ListView list=(ListView) getActivity().findViewById(R.id.listaEventos);
            if(json.equals("33")) Toast.makeText(getActivity(), "Vacio", Toast.LENGTH_SHORT).show();
            else if(json.equals("[]") || json=="12") Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
            else {
                Gson gson = new Gson();
                recive = gson.fromJson(json.replace("fecha_inicio", "fechainicio").replace("fecha_fin", "fechafin").replace("titulo", "nombre"), new TypeToken<List<Evento>>() {}.getType());
                System.out.println();
                if (recive != null) {
                    /*String[] sistemas = {"Ubuntu", "Android", "iOS", "Windows", "Mac OSX",
                            "Google Chrome OS", "Debian", "Mandriva", "Solaris", "Unix", "Android", "iOS", "Windows", "Mac OSX",
                            "Google Chrome OS", "Debian", "Mandriva", "Solaris", "Unix"};*/
                    //final String[] sistemas = new String[recive.size()];

                    listDataHeader = new ArrayList<String>();
                    listDataChild = new HashMap<String, LinkedList<Evento>>();

                    // Adding child data
                    /*listDataHeader.add("Universidad");
                    listDataHeader.add("Campus");
                    listDataHeader.add("Facultad");
                    listDataHeader.add("Carrera");
                    listDataHeader.add("Materias");*/
                    final LinkedList<Evento> universidad = new LinkedList<Evento>();
                    final LinkedList<Evento> campus = new LinkedList<Evento>();
                    final LinkedList<Evento> facultad = new LinkedList<Evento>();
                    final LinkedList<Evento> carrera = new LinkedList<Evento>();
                    final LinkedList<Evento> materia = new LinkedList<Evento>();
                    for (Evento objeto : recive) {
                        if(objeto.getId_facultad()!=0){
                            if(!listDataHeader.contains("Facultad"))
                                listDataHeader.add("Facultad");
                            facultad.add(objeto);
                        }
                        else if(objeto.getId_universidad()!=0){
                            if(!listDataHeader.contains("Universidad"))
                                listDataHeader.add("Universidad");
                            universidad.add(objeto);
                        }
                        else if(objeto.getId_campus()!=0){
                            if(!listDataHeader.contains("Campus"))
                                listDataHeader.add("Campus");
                            campus.add(objeto);
                        }
                        else if(objeto.getId_carrera()!=0){
                            if(!listDataHeader.contains("Carrera"))
                                listDataHeader.add("Carrera");
                            carrera.add(objeto);
                        }
                        else if(objeto.getId_materia()!=0){
                            if(!listDataHeader.contains("Materia"))
                                listDataHeader.add("Materia");
                            materia.add(objeto);
                        }
                        /*sistemas[count] = objeto.getNombre();
                        count += 1;*/
                    }

                    if(listDataHeader.contains("Universidad"))
                    listDataChild.put(listDataHeader.get(listDataHeader.indexOf("Universidad")), universidad); // Header, Child data
                    if(listDataHeader.contains("Campus"))
                    listDataChild.put(listDataHeader.get(listDataHeader.indexOf("Campus")), campus);
                    if(listDataHeader.contains("Facultad"))
                    listDataChild.put(listDataHeader.get(listDataHeader.indexOf("Facultad")), facultad);
                    if(listDataHeader.contains("Carrera"))
                    listDataChild.put(listDataHeader.get(listDataHeader.indexOf("Carrera")), carrera);
                    if(listDataHeader.contains("Materia"))
                    listDataChild.put(listDataHeader.get(listDataHeader.indexOf("Materia")), materia);
                    listAdapter = new ExpandableListAdapter(getContext(), listDataHeader, listDataChild);
                    // setting list adapter*/

                    /*Thread thread = new Thread() {
                        @Override
                        public void run() {
                            try {
                                final ArrayAdapter<String> adaptador = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, sistemas);
                                getActivity().runOnUiThread(new Runnable() {
                                    public void run() {
                                        list.setAdapter(adaptador);
                                    }

                                });
                                sleep(500);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    thread.start();*/
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                expListView.setAdapter(listAdapter);
                            }
                        });
                    }
            }
            pDialog.dismiss();
            expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                @Override
                public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                    FragmentTres x = new FragmentTres();
                    Bundle bundle = new Bundle();
                    Gson gson=new Gson();
                    String Evento=gson.toJson(listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition));
                    bundle.putString("Evento",Evento);
                    x.setArguments(bundle);
                    getFragmentManager().beginTransaction().replace(R.id.Contendedor, x).addToBackStack(null).commit();
                    return true;
                }
            });
            /*list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    FragmentTres x = new FragmentTres();
                    Bundle bundle = new Bundle();
                    //bundle.putInt("dia", dayOfMonth);
                    Gson gson=new Gson();
                    String Evento=gson.toJson(recive.get(position));
                    bundle.putString("Evento",Evento);
                    x.setArguments(bundle);
                    getFragmentManager().beginTransaction().replace(R.id.Contendedor, x).commit();
                }
            });*/
        }
    }
}
