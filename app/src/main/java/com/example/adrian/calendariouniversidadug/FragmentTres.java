package com.example.adrian.calendariouniversidadug;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.adrian.calendariouniversidadug.calendar.Evento;
import com.example.adrian.calendariouniversidadug.calendar.EventosSQL;
import com.google.gson.Gson;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentTres.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentTres#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentTres extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentTres() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentTres.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentTres newInstance(String param1, String param2) {
        FragmentTres fragment = new FragmentTres();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_fragment_tres, container, false);
        String Evento= getArguments().getString("Evento");
        Gson gson=new Gson();
        Evento recive=gson.fromJson(Evento,Evento.class);
        final TextView nombre=(TextView) v.findViewById(R.id.NombreEvento);
        TextView tipo=(TextView) v.findViewById(R.id.TipoEvento);
        TextView descripcion=(TextView) v.findViewById(R.id.DescripcionEvento);
        final TextView clasesBool=(TextView) v.findViewById(R.id.ExisteClase);
        Button compartir=(Button) v.findViewById(R.id.Compartir);
        nombre.setText(recive.getNombre());
        if(recive.clases==1){
            clasesBool.setText("NO hay clases");
        }
        else {
            clasesBool.setText("SI hay clases");
        }

        EventosSQL usdbh = new EventosSQL(getContext(), "DBCalendar", null, 1);
        final SQLiteDatabase db = usdbh.getWritableDatabase();
        descripcion.setText(recive.getDescripcion());
        if(recive.getId_universidad()!=0){
            Cursor cursorUniversidad = db.rawQuery("select nombre_universidad from ug_universidad where id_universidad=" + recive.getId_universidad(), null);//aqui se debe aumentar logica
            if (cursorUniversidad != null) {
                cursorUniversidad.moveToFirst();
                tipo.setText(cursorUniversidad.getString(0));
            }
        }
        else if(recive.getId_campus()!=0){
            Cursor cursorCampus = db.rawQuery("select nombre_campus from ug_campus where id_campus=" + recive.getId_campus(), null);//aqui se debe aumentar logica
            if (cursorCampus != null) {
                cursorCampus.moveToFirst();
                tipo.setText(cursorCampus.getString(0));
            }
        }
        else if(recive.getId_facultad()!=0){
            Cursor cursorFacultad = db.rawQuery("select nombre_facultad from ug_facultad where id_facultad=" + recive.getId_facultad(), null);//aqui se debe aumentar logica
            if (cursorFacultad != null) {
                cursorFacultad.moveToFirst();
                tipo.setText(cursorFacultad.getString(0));
            }
        }
        else if(recive.getId_materia()!=0){
            Cursor cursorMateria = db.rawQuery("select nombre from ug_materia where id_materia=" + recive.getId_materia(), null);//aqui se debe aumentar logica
            if (cursorMateria != null) {
                cursorMateria.moveToFirst();
                tipo.setText(cursorMateria.getString(0));
            }
        }
        else if(recive.getId_carrera()!=0){
            Cursor cursorCarrera = db.rawQuery("select nombre_carrera from ug_carrera where id_carrera=" + recive.getId_carrera(), null);//aqui se debe aumentar logica
            if (cursorCarrera != null) {
                cursorCarrera.moveToFirst();
                tipo.setText(cursorCarrera.getString(0));
            }
        }

        compartir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //codigo solo por cumplimiento
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, "El evento de "+nombre.getText()+" nos indica que "+clasesBool.getText());
                startActivity(Intent.createChooser(intent, "Compartir con"));
            }
        });

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
