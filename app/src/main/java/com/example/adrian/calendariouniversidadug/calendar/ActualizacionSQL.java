package com.example.adrian.calendariouniversidadug.calendar;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by adrian on 02/12/17.
 */

public class ActualizacionSQL {

    public void VER(Context context, Integer cedula) {
        Calendar calendar = Calendar.getInstance();
        Date date = new Date();//hoy
        calendar.setTime(date);
        calendar.add(Calendar.MONTH,-1);
        //SimpleDateFormat formato=new SimpleDateFormat("dd-MM-yy");
        date = calendar.getTime();
        String dia = new SimpleDateFormat("yy/MM/dd").format(date);
        String[] fechaminima=dia.split("/");
        final int borrarhasta=Integer.parseInt(1+fechaminima[0]+fechaminima[1]+fechaminima[2]);
        /*try {
            date=formato.parse(dia);
        } catch (ParseException e) {
            e.printStackTrace();
        }*/
        EventosSQL usdbh = new EventosSQL(context, "DBCalendar", null, 1);
        final SQLiteDatabase db = usdbh.getWritableDatabase();
        if (db != null) {
            int estado=EstadoTabla("ug_eventos","id_evento",db);
            new ConsumoActualizacion.eventos(new ConsumoActualizacion.eventos.AsyncResponse() {
                @Override
                public void processFinish(List<Evento> output) {
                    String clausulaWhere="fecha_fin<"+borrarhasta;
                    db.delete("ug_eventos",clausulaWhere, null);//esta linea esta mal
                    if (output.isEmpty()) {
                    } else {
                        for (Evento auxiliar: output) {
                            String[] ini=auxiliar.getFechainicio().split("/");
                            String[] fin=auxiliar.getFechafin().split("/");
                            int inicio=Integer.parseInt(1+ini[2]+ini[1]+ini[0]);
                            int fina=Integer.parseInt(1+fin[2]+fin[1]+fin[0]);
                            db.execSQL("INSERT INTO ug_eventos (id_evento, id_universidad, id_campus, id_facultad, " +
                                    "id_carrera, id_materia, fecha_inicio, fecha_fin, " +
                                    "titulo, descripcion, clases) VALUES ("+ auxiliar.getId_evento()+","+ auxiliar.getId_universidad()+"," +
                                     auxiliar.getId_campus()+","+ auxiliar.getId_facultad()+"," +
                                     auxiliar.getId_carrera()+","+ auxiliar.getId_materia()+"," +
                                     inicio+","+ fina+",'" +
                                     auxiliar.getNombre()+"','"+ auxiliar.getDescripcion()+"'," +
                                     auxiliar.getClases()+" )");
                        }
                    }
                }
            }).execute(estado);

            estado=EstadoTabla("ug_feriados","id_feriado",db);
            new ConsumoActualizacion.feriados(new ConsumoActualizacion.feriados.AsyncResponse() {
                @Override
                public void processFinish(List<Feriado> output) {
                    if (output.isEmpty()) {
                    } else{
                        for (Feriado auxiliar: output) {
                            String[] ini=auxiliar.getFechainicio().split("/");
                            String[] fin=auxiliar.getFechafin().split("/");
                            int inicio=Integer.parseInt(1+ini[1]+ini[0]);
                            int fina=Integer.parseInt(1+fin[1]+fin[0]);
                            db.execSQL("INSERT INTO ug_feriados (id_feriado, id_ciudad, fecha_inicio," +
                                    "fecha_fin, clases, titulo, descripcion) VALUES ("+ auxiliar.getId_feriado()+"," +
                                    auxiliar.getId_ciudad() +","+ inicio+","+ fina+"," +
                                    auxiliar.getClases()+",'"+auxiliar.getNombre()+"','" +
                                    auxiliar.getDescripcion()+"')");
                        }
                    }
                }
            }).execute(estado);

            estado=EstadoTabla("ug_campus","id_campus",db);
            new ConsumoActualizacion.campus(new ConsumoActualizacion.campus.AsyncResponse() {
                @Override
                public void processFinish(List<Campus> output) {
                    if (output.isEmpty()) {
                    } else {
                        for (Campus auxiliar : output) {
                            db.execSQL("INSERT INTO ug_campus (id_campus, id_universidad, nombre_campus, encargado) VALUES (" +
                                    +auxiliar.getId_campus() + "," + auxiliar.getId_universidad() + ",'"
                                    + auxiliar.getNombre() + "'," + auxiliar.getEncargado() + ")");
                        }
                    }
                }
            }).execute(estado);

            estado=EstadoTabla("ug_carrera","id_carrera",db);
            new ConsumoActualizacion.carrera(new ConsumoActualizacion.carrera.AsyncResponse() {
                @Override
                public void processFinish(List<Carrera> output) {
                    if (output.isEmpty()) {
                    } else{
                        for (Carrera auxiliar : output) {
                            db.execSQL("INSERT INTO ug_carrera (id_carrera, id_facultad, nombre_carrera, encargado) VALUES (" +
                                    +auxiliar.getId_carrera() + "," + auxiliar.getId_facultad() + ",'"
                                    + auxiliar.getNombre() + "'," + auxiliar.getEncargado() + ")");
                        }
                    }
                }
            }).execute(estado);

            estado=EstadoTabla("ug_ciudad","id_ciudad",db);
            new ConsumoActualizacion.ciudades(new ConsumoActualizacion.ciudades.AsyncResponse() {
                @Override
                public void processFinish(List<Ciudad> output) {
                    if (output.isEmpty()) {
                    } else{
                        for (Ciudad auxiliar : output) {
                            db.execSQL("INSERT INTO ug_ciudad (id_ciudad, id_pais, nombre) VALUES (" +
                                    +auxiliar.getId_ciudad() + "," + auxiliar.getId_pais() + ",'"
                                    + auxiliar.getNombre() + "')");
                        }
                    }
                }
            }).execute(estado);

            estado=EstadoTabla("ug_curso","id_curso",db);
            new ConsumoActualizacion.cursos(new ConsumoActualizacion.cursos.AsyncResponse() {
                @Override
                public void processFinish(List<Cursos> output) {
                    if (output.isEmpty()) {
                    } else{
                        for (Cursos auxiliar : output) {
                            db.execSQL("INSERT INTO ug_curso (id_curso, modalidad, nombre) VALUES (" +
                                    +auxiliar.getId_curso() + ",'" + auxiliar.getModalidad() + "','"
                                    + auxiliar.getNombre() + "')");
                        }
                    }
                }
            }).execute(estado);

            estado=EstadoTabla("ug_facultad","id_facultad",db);
            new ConsumoActualizacion.facultad(new ConsumoActualizacion.facultad.AsyncResponse() {
                @Override
                public void processFinish(List<Facultad> output) {
                    if (output.isEmpty()) {
                    } else{
                        for (Facultad auxiliar : output) {
                            db.execSQL("INSERT INTO ug_facultad (id_facultad, id_campus, nombre_facultad, encargado) VALUES (" +
                                    +auxiliar.getId_facultad() + "," + auxiliar.getId_campus() + ",'"
                                    + auxiliar.getNombre() + "'," + auxiliar.getEncargado() + ")");
                        }
                    }
                }
            }).execute(estado);

            estado=EstadoTabla("ug_materia","id_materia",db);
            new ConsumoActualizacion.materia(new ConsumoActualizacion.materia.AsyncResponse() {
                @Override
                public void processFinish(List<Materia> output) {
                    if (output.isEmpty()) {
                    } else {
                        for (Materia auxiliar : output) {
                            db.execSQL("INSERT INTO ug_materia (id_materia, cedula, id_curso, nombre, semestre, id_carrera) VALUES (" +
                                    +auxiliar.getId_materia() + "," + auxiliar.getCedula() + "," + auxiliar.getId_curso() + ",'"
                                    +auxiliar.getNombre() + "',"+auxiliar.getSemestre()+","+auxiliar.getId_carrera()+")");
                        }
                    }
                }
            }).execute(estado);

            estado=EstadoTabla("ug_pais","id_pais",db);
            new ConsumoActualizacion.pais(new ConsumoActualizacion.pais.AsyncResponse() {
                @Override
                public void processFinish(List<Pais> output) {
                    if (output.isEmpty()) {
                    } else {
                        for (Pais auxiliar : output) {
                            db.execSQL("INSERT INTO ug_pais (id_pais, nombre) VALUES (" +
                                    +auxiliar.getId_pais() + ",'" + auxiliar.getNombre() + "')");
                        }
                    }
                }
            }).execute(estado);

            estado=EstadoTabla("ug_ubicacion_facultad","id_ubicacion",db);
            new ConsumoActualizacion.ubicacion_facultad(new ConsumoActualizacion.ubicacion_facultad.AsyncResponse() {
                @Override
                public void processFinish(List<Ubicacion> output) {
                    if (output.isEmpty()) {
                    } else {
                        for (Ubicacion auxiliar : output) {
                            db.execSQL("INSERT INTO ug_ubicacion_facultad (id_ubicacion, id_facultad, longitud, " +
                                    "latitud) VALUES (" + auxiliar.getId_ubicacion() + "," + auxiliar.getId_facultad()
                                    + "," +Double.parseDouble(auxiliar.getLongitud()) + ","+Double.parseDouble(auxiliar.getLatitud())+")");
                        }
                    }
                }
            }).execute(estado);

            estado=EstadoTabla("ug_universidad","id_universidad",db);
            new ConsumoActualizacion.universidad(new ConsumoActualizacion.universidad.AsyncResponse() {
                @Override
                public void processFinish(List<Universidad> output) {
                    if (output.isEmpty()) {
                    } else {
                        for (Universidad auxiliar : output) {
                            db.execSQL("INSERT INTO ug_universidad (id_universidad, id_ciudad, nombre_universidad, encargado) " +
                                    " VALUES (" + auxiliar.getId_universidad() + "," + auxiliar.getId_ciudad()
                                    + ",'" +auxiliar.getNombre() + "'," + auxiliar.getEncargado() + ")");
                        }
                    }
                }
            }).execute(estado);

            estado=EstadoTabla("ug_matricula","id_matricula",db);
            new ConsumoActualizacion.matricula(new ConsumoActualizacion.matricula.AsyncResponse() {
                @Override
                public void processFinish(Matricula output) {
                    if(output.equals(null)|| output.equals("")){
                    }else {
                        db.execSQL("delete from ug_matricula");
                        db.execSQL("INSERT INTO ug_matricula (id_matricula, cedula, id_carrera, fecha," +
                                "fecha_exp, id_materia1, id_materia2, id_materia3, id_materia4" +
                                ", id_materia5, id_materia6, id_materia7, id_materia8, id_materia9) VALUES" +
                                "("+output.getId_matricula()+","+output.getCedula()+","+output.getId_carrera()+
                                ",'"+output.getFecha()+"','"+output.getFecha_exp()+"',"+output.getId_materia1()+
                                ","+output.getId_materia2()+","+output.getId_materia3()+","+output.getId_materia4()+
                                ","+output.getId_materia5()+","+output.getId_materia6()+","+output.getId_materia7()+
                                ","+output.getId_materia8()+","+output.getId_materia9()+")");
                    }
                }
            }).execute(cedula, estado);
        }
        else {
            Toast.makeText(context, "Poco espacio de memoria", Toast.LENGTH_SHORT).show();
        }
        //select * from ug_feriados where id_feriado>select max(id_feriado) from ug_feriados
        //select * from ug_eventos where id_evento>select max(id_evento) from ug_eventos
        //select * from ug_campus where id_campus>select max(id_campus) from ug_campus
        //select * from ug_carrera where id_carrera>select max(id_carrera) from ug_carrera
        //select * from ug_ciudad where id_ciudad>select max(id_ciudad) from ug_ciudad
        //select * from ug_curso where id_curso>select max(id_curso) from ug_curso
        //select * from ug_facultad where id_facultad>select max(id_facultad) from ug_facultad
        //select * from ug_materia where id_materia>select max(id_materia) from ug_materia
        //select * from ug_pais where id_pais>select max(id_pais) from ug_pais
        //select * from ug_ubicacion_facultad where id_ubicacion>select max(id_ubicacion) from ug_ubicacion_facultad
        //select * from ug_universidad where id_universidad>select max(id_universidad) from ug_universidad
    }

    public int EstadoTabla(String tabla, String id_tabla, SQLiteDatabase db) {
        Cursor cursor = db.rawQuery("select max(" + id_tabla + ") from " + tabla, null);
        int id=0;
        if (cursor != null && !cursor.isClosed()) {
            if(cursor.moveToFirst())
                id=cursor.getInt(0);
            cursor.close();
        }
        return id;
    }
}
