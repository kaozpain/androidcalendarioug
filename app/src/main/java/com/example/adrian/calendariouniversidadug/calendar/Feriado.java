package com.example.adrian.calendariouniversidadug.calendar;

/**
 * Created by adrian on 03/12/17.
 */

public class Feriado {
    public int id_feriado=0;
    public int id_ciudad=0;
    public int id_pais=0;
    public String fechainicio=null;
    public String fechafin=null;
    public int clases=0;
    public String nombre=null;
    public String descripcion=null;

    public int getId_feriado() {
        return id_feriado;
    }

    public void setId_feriado(int id_feriado) {
        this.id_feriado = id_feriado;
    }

    public int getId_ciudad() {
        return id_ciudad;
    }

    public void setId_ciudad(int id_ciudad) {
        this.id_ciudad = id_ciudad;
    }

    public int getId_pais() {
        return id_pais;
    }

    public void setId_pais(int id_pais) {
        this.id_pais = id_pais;
    }

    public String getFechainicio() {
        return fechainicio;
    }

    public void setFechainicio(String fechainicio) {
        this.fechainicio = fechainicio;
    }

    public String getFechafin() {
        return fechafin;
    }

    public void setFechafin(String fechafin) {
        this.fechafin = fechafin;
    }

    public int getClases() {
        return clases;
    }

    public void setClases(int clases) {
        this.clases = clases;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
