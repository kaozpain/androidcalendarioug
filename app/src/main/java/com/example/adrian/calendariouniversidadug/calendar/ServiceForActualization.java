package com.example.adrian.calendariouniversidadug.calendar;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public class ServiceForActualization extends Service {

    private Timer temporizador = new Timer();
    private static final long INTERVALO_ACTUALIZACION = 180000; // 3 min En ms 1 min=60000
    long INTERVALO_NOTIFICACION= 86400000;

    public ServiceForActualization() {
    }

    @Override
    public void onCreate(){
        super.onCreate();
        //Toast.makeText(this, "Service created", Toast.LENGTH_SHORT).show();
    }
    

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final int cedula=intent.getIntExtra("cedula", 0);
        Toast.makeText(this, "Service started", Toast.LENGTH_SHORT).show();
        temporizador.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                ActualizacionSQL aux=new ActualizacionSQL();
                aux.VER(ServiceForActualization.this, cedula);
                //llamar la actualizaion
            }
        }, 0, INTERVALO_ACTUALIZACION);
        final SharedPreferences auxiliar= PreferenceManager.getDefaultSharedPreferences(this);
        final Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 6);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        temporizador.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if(auxiliar.getBoolean("recibe_notificaciones", true)) {
                        Notificaciones aux1=new Notificaciones();
                        aux1.enviar(ServiceForActualization.this);
                }
            }
        }, c.getTimeInMillis(), INTERVALO_NOTIFICACION);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //Toast.makeText(this, "Service destroy", Toast.LENGTH_SHORT).show();
        //temporizador.cancel();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
