package com.example.adrian.calendariouniversidadug;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.adrian.calendariouniversidadug.calendar.Usuario;
import com.google.gson.Gson;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpResponseException;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

public class Registro extends AppCompatActivity {

    private TextView lblGotoLogin;
    private Button btnRegister;
    private EditText inputUserName;
    private EditText inputEmail;
    private EditText inputPassword;
    private EditText inputPassword2;
    private TextView registerErrorMsg;

    Double latitude,longitud,altitud;
    Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        inputUserName = (EditText) findViewById(R.id.txtUserName);
        inputEmail = (EditText) findViewById(R.id.txtEmail);
        inputPassword = (EditText) findViewById(R.id.txtPass);
        inputPassword2 = (EditText) findViewById(R.id.txtPass2);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        registerErrorMsg = (TextView) findViewById(R.id.register_error);
        lblGotoLogin=(TextView) findViewById(R.id.link_to_login);
        //Toast.makeText(registro1.this,latitude+" "+longitud+" "+altitud,Toast.LENGTH_LONG).show();

        btnRegister.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                String correo=inputEmail.getText().toString().replace(" ","");
                String usuario=inputUserName.getText().toString().replace(" ","");
                String pass1=inputPassword.getText().toString().replace(" ","");
                String pass2=inputPassword2.getText().toString().replace(" ","");
                if(correo.equalsIgnoreCase("")||usuario.equalsIgnoreCase("")||pass1.equalsIgnoreCase("")||pass2.equalsIgnoreCase("")){
                    if (correo.equalsIgnoreCase("")){
                        inputEmail.setError("Campo no puede estar vacio");
                    }
                    if (usuario.equalsIgnoreCase("")){
                        inputUserName.setError("Campo no puede estar vacio");
                    }
                    if (pass1.equalsIgnoreCase("")){
                        inputPassword.setError("Campo no puede estar vacio");
                    }
                    if(pass2.equalsIgnoreCase("")){
                        inputPassword2.setError("Campo no puede estar vacio");
                    }
                }
                else if(pass1.length()<6){
                    inputPassword.setError("Contraseña debe tener minimo 6 caracteres");
                }
                else {
                    if (!pass1.equals(pass2)) {
                        inputPassword2.setError("Contraseña no coincide");
                    } else {
                        //aqui va verificacion de correo y llamas al ws
                        if (correo.contains("@")) {
                            operacionSoap WSllamada=new operacionSoap();
                            WSllamada.execute(pass1, usuario, correo);
                        }
                        else {
                            inputEmail.setError("Correo no reconocido");
                        }
                    }
                }
            }
        });

        lblGotoLogin.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                Intent intent = new Intent (view.getContext(), com.example.adrian.calendariouniversidadug.Login.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivityForResult(intent, 0);
            }
        });

    }

    ProgressDialog pDialog;
    public class operacionSoap extends AsyncTask<String,String,Integer> {

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

            pDialog = new ProgressDialog(Registro.this);
            pDialog.setMessage("Verificando datos");
            pDialog.setCancelable(false);
            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pDialog.show();
        }

        @Override
        protected Integer doInBackground(String... params) {
            int codigo=25;
            final String NAMESPACE="http://tempuri.org/";
            final String METHODNAME="ExisteCliente";
            //final String URL="http://192.168.1.4:8086/prueba1.asmx";
            //final String URL="http://192.168.1.130:8086/prueba1.asmx";
            final String URL=getString(R.string.ip);//casa de abuelita
            final String SOAPACTION=NAMESPACE+METHODNAME;
            SoapObject request=new SoapObject(NAMESPACE, METHODNAME);
            //request.addProperty("cedula",params[0]);
            request.addProperty("usuario",params[1]);
            request.addProperty("correo",params[2]);
            //request.addProperty("tipo",params[3]);
            SoapSerializationEnvelope envelope=new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet=true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE transporte=new HttpTransportSE(URL);
            Log.d("Transporte ", request.toString());
            try{
                transporte.call(SOAPACTION, envelope);
                //SoapObject response=(SoapObject) envelope.bodyIn;

                SoapPrimitive response=(SoapPrimitive) envelope.getResponse();
                codigo=Integer.parseInt(response.toString());
                if(codigo==8) {
                    EnviarOTP aux=new EnviarOTP();
                    aux.execute(params[1], params[2]);
                }
                //gson=new Gson();
                //recive=gson.fromJson(strJson,Usuario.class);
                /*
                List<Usuario> recive=gson.fromJson(strJson,new TypeToken<List<Usuario>>(){}.getType());
                System.out.println();
                if( recive!= null ){
                    for(Usuario object : recive){
                        System.out.println("\nAlumno : " + object.getTipo() + " " + object.getEmail() );
                    }
                }*/
                //Toast.makeText(Registro.this,strJson,Toast.LENGTH_LONG).show();
                return codigo;
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (SoapFault soapFault) {
                soapFault.printStackTrace();
            } catch (HttpResponseException e) {
                codigo=33;
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e){
                e.printStackTrace();
            }
            return codigo;
        }

        @Override
        protected void onPostExecute(Integer codigo) {
            super.onPostExecute(codigo);
            //usuario="adrian272566@hotmail.es";
            //Toast.makeText(Registro.this,usuario,Toast.LENGTH_LONG).show();//usuario.cedula+" "+usuario.email+" "+usuario.tipo+" "+usuario.nombre,Toast.LENGTH_LONG).show();
            //EnvioAlCorreo mail=new EnvioAlCorreo();
            //mail.enviar(usuario.correo);
            //mail.enviar(usuario.email);
                if(codigo==8){}
                else {
                    if (codigo == 10)
                        Toast.makeText(Registro.this, "Correo no registrado en la universidad", Toast.LENGTH_SHORT).show();
                    else if (codigo == 12)
                        Toast.makeText(Registro.this, "Usuario no disponible", Toast.LENGTH_SHORT).show();
                    else if (codigo == 22)
                        Toast.makeText(Registro.this, "Usted ya se encuentra registrado en esta aplicacion", Toast.LENGTH_SHORT).show();
                    else if (codigo == 33)
                        Toast.makeText(Registro.this, "Porfavor intentelo mas tarde", Toast.LENGTH_SHORT).show();
                    else if (codigo == 25)
                        Toast.makeText(Registro.this, "Revise su conexion a internet y vuelva a intentarlo", Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(Registro.this, "Error desconocido, si el problema persiste consulte a su universidad", Toast.LENGTH_SHORT).show();
                    pDialog.dismiss();
                }
            //Toast.makeText(Registro.this, "Correo enviado a "+usuario.getCorreo(), Toast.LENGTH_SHORT).show();
        }
    }


    public class EnviarOTP extends AsyncTask<String,String,Integer> {

        /*ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

            pDialog = new ProgressDialog(Registro.this);
            pDialog.setMessage("Verificando datos");
            pDialog.setCancelable(true);
            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pDialog.show();
        }*/

        @Override
        protected Integer doInBackground(String... params) {
            int codigo=25;
            final String NAMESPACE="http://tempuri.org/";
            final String METHODNAME="EnviarOTP";
            //final String URL="http://192.168.1.4:8086/prueba1.asmx";
            //final String URL="http://192.168.1.130:8086/prueba1.asmx";
            final String URL=getString(R.string.ip);//casa de abuelita
            final String SOAPACTION=NAMESPACE+METHODNAME;
            SoapObject request=new SoapObject(NAMESPACE, METHODNAME);
            //request.addProperty("cedula",params[0]);
            request.addProperty("usuario",params[0]);
            request.addProperty("correo",params[1]);
            //request.addProperty("tipo",params[3]);
            SoapSerializationEnvelope envelope=new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet=true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE transporte=new HttpTransportSE(URL);
            Log.d("Transporte ", request.toString());
            try{
                transporte.call(SOAPACTION, envelope);
                //SoapObject response=(SoapObject) envelope.bodyIn;

                SoapPrimitive response=(SoapPrimitive) envelope.getResponse();
                codigo=Integer.parseInt(response.toString());
                //gson=new Gson();
                //recive=gson.fromJson(strJson,Usuario.class);
                /*
                List<Usuario> recive=gson.fromJson(strJson,new TypeToken<List<Usuario>>(){}.getType());
                System.out.println();
                if( recive!= null ){
                    for(Usuario object : recive){
                        System.out.println("\nAlumno : " + object.getTipo() + " " + object.getEmail() );
                    }
                }*/
                //Toast.makeText(Registro.this,strJson,Toast.LENGTH_LONG).show();
                return codigo;
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (SoapFault soapFault) {
                soapFault.printStackTrace();
            } catch (HttpResponseException e) {
                codigo=33;
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e){
                e.printStackTrace();
            }
            return codigo;
        }

        @Override
        protected void onPostExecute(Integer codigo) {
            super.onPostExecute(codigo);
            if(codigo==120){
                Toast.makeText(Registro.this, "Envio de codigo a su correo", Toast.LENGTH_SHORT).show();
                final Dialog commentDialog = new Dialog(Registro.this);
                commentDialog.setContentView(R.layout.codigomodal);
                commentDialog.setTitle("Ingrese codigo");
                commentDialog.show();
                commentDialog.setCancelable(false);
                Button okBtn = (Button) commentDialog.findViewById(R.id.ok);
                final EditText codigotxt=(EditText) commentDialog.findViewById(R.id.body);
                okBtn.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if(codigotxt.getText().length()==0) codigotxt.setError("Codigo no puede estar vacio");
                        else{
                            if(codigotxt.getText().length()!=6) codigotxt.setError("Codigo no valido");
                            else{
                                ValidarOTP x=new ValidarOTP();
                                x.execute(codigotxt.getText().toString().trim(), inputEmail.getText().toString().trim());
                            }
                        }
                        //commentDialog.dismiss();
                    }
                });
            }
            else if(codigo==99) Toast.makeText(Registro.this, "Error al enviar al correo, porfavor intente mas tarde", Toast.LENGTH_SHORT).show();
            else if(codigo==33) Toast.makeText(Registro.this, "Porfavor intentelo mas tarde", Toast.LENGTH_SHORT).show();
            else if(codigo==25) Toast.makeText(Registro.this, "Revise su conexion a internet y vuelva a intentarlo", Toast.LENGTH_SHORT).show();
            else Toast.makeText(Registro.this, "Error desconocido, si el problema persiste consulte a su universidad", Toast.LENGTH_SHORT).show();
            //usuario="adrian272566@hotmail.es";
            //Toast.makeText(Registro.this,usuario,Toast.LENGTH_LONG).show();//usuario.cedula+" "+usuario.email+" "+usuario.tipo+" "+usuario.nombre,Toast.LENGTH_LONG).show();
            //EnvioAlCorreo mail=new EnvioAlCorreo();
            //mail.enviar(usuario.correo);
            //mail.enviar(usuario.email);
            //pDialog.dismiss();
            //Toast.makeText(Registro.this, "Correo enviado a "+usuario.getCorreo(), Toast.LENGTH_SHORT).show();
            pDialog.dismiss();
        }
    }

    public class ValidarOTP extends AsyncTask<String,String,Integer> {

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

            pDialog = new ProgressDialog(Registro.this);
            pDialog.setMessage("Verificando codigo");
            pDialog.setCancelable(false);
            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pDialog.show();
        }

        @Override
        protected Integer doInBackground(String... params) {
            int codigo=25;
            final String NAMESPACE="http://tempuri.org/";
            final String METHODNAME="ValidarOTP";
            //final String URL="http://192.168.1.4:8086/prueba1.asmx";
            //final String URL="http://192.168.1.130:8086/prueba1.asmx";
            final String URL=getString(R.string.ip);//casa de abuelita
            final String SOAPACTION=NAMESPACE+METHODNAME;
            SoapObject request=new SoapObject(NAMESPACE, METHODNAME);
            request.addProperty("codigo",params[0]);
            request.addProperty("correo",params[1]);
            SoapSerializationEnvelope envelope=new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet=true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE transporte=new HttpTransportSE(URL);
            Log.d("Transporte ", request.toString());
            try{
                transporte.call(SOAPACTION, envelope);
                //SoapObject response=(SoapObject) envelope.bodyIn;

                SoapPrimitive response=(SoapPrimitive) envelope.getResponse();
                codigo=Integer.parseInt(response.toString());
                /*if(codigo==8) {
                    EnviarOTP aux=new EnviarOTP();
                    aux.execute(params[1], params[2]);
                }
                //gson=new Gson();
                //recive=gson.fromJson(strJson,Usuario.class);
                /*
                List<Usuario> recive=gson.fromJson(strJson,new TypeToken<List<Usuario>>(){}.getType());
                System.out.println();
                if( recive!= null ){
                    for(Usuario object : recive){
                        System.out.println("\nAlumno : " + object.getTipo() + " " + object.getEmail() );
                    }
                }*/
                //Toast.makeText(Registro.this,strJson,Toast.LENGTH_LONG).show();
                return codigo;
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (SoapFault soapFault) {
                soapFault.printStackTrace();
            } catch (HttpResponseException e) {
                codigo=33;
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e){
                e.printStackTrace();
            }
            return codigo;
        }

        @Override
        protected void onPostExecute(Integer codigo) {
            super.onPostExecute(codigo);
            //usuario="adrian272566@hotmail.es";
            //Toast.makeText(Registro.this,usuario,Toast.LENGTH_LONG).show();//usuario.cedula+" "+usuario.email+" "+usuario.tipo+" "+usuario.nombre,Toast.LENGTH_LONG).show();
            //EnvioAlCorreo mail=new EnvioAlCorreo();
            //mail.enviar(usuario.correo);
            //mail.enviar(usuario.email);
            if(codigo==8){
                RegistrarUser aux=new RegistrarUser();
                aux.execute(inputUserName.getText().toString(), inputPassword.getText().toString(), inputEmail.getText().toString());
            }
            else{
                if(codigo==12) Toast.makeText(Registro.this, "Codigo Ingresado Incorrecto", Toast.LENGTH_SHORT).show();
                else if(codigo==33) Toast.makeText(Registro.this, "Porfavor intentelo mas tarde", Toast.LENGTH_SHORT).show();
                else if(codigo==25) Toast.makeText(Registro.this, "Revise su conexion a internet y vuelva a intentarlo", Toast.LENGTH_SHORT).show();
                else Toast.makeText(Registro.this, "Error desconocido, si el problema persiste consulte a su universidad", Toast.LENGTH_SHORT).show();
                pDialog.dismiss();
            }
            //Toast.makeText(Registro.this, "Correo enviado a "+usuario.getCorreo(), Toast.LENGTH_SHORT).show();
        }
    }

    public class RegistrarUser extends AsyncTask<String,String,Integer> {

        ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(String... params) {
            int codigo=25;
            final String NAMESPACE="http://tempuri.org/";
            final String METHODNAME="registro";
            //final String URL="http://192.168.1.4:8086/prueba1.asmx";
            //final String URL="http://192.168.1.130:8086/prueba1.asmx";
            final String URL=getString(R.string.ip);//casa de abuelita
            final String SOAPACTION=NAMESPACE+METHODNAME;
            SoapObject request=new SoapObject(NAMESPACE, METHODNAME);
            request.addProperty("usuario",params[0]);
            request.addProperty("clave",params[1]);
            request.addProperty("correo",params[2]);
            SoapSerializationEnvelope envelope=new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet=true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE transporte=new HttpTransportSE(URL);
            Log.d("Transporte ", request.toString());
            try{
                transporte.call(SOAPACTION, envelope);
                //SoapObject response=(SoapObject) envelope.bodyIn;

                SoapPrimitive response=(SoapPrimitive) envelope.getResponse();
                codigo=Integer.parseInt(response.toString());
                /*if(codigo==8) {
                    EnviarOTP aux=new EnviarOTP();
                    aux.execute(params[1], params[2]);
                }
                //gson=new Gson();
                //recive=gson.fromJson(strJson,Usuario.class);
                /*
                List<Usuario> recive=gson.fromJson(strJson,new TypeToken<List<Usuario>>(){}.getType());
                System.out.println();
                if( recive!= null ){
                    for(Usuario object : recive){
                        System.out.println("\nAlumno : " + object.getTipo() + " " + object.getEmail() );
                    }
                }*/
                //Toast.makeText(Registro.this,strJson,Toast.LENGTH_LONG).show();
                return codigo;
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (SoapFault soapFault) {
                soapFault.printStackTrace();
            } catch (HttpResponseException e) {
                codigo=33;
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e){
                e.printStackTrace();
            }
            return codigo;
        }

        @Override
        protected void onPostExecute(Integer codigo) {
            super.onPostExecute(codigo);
            //usuario="adrian272566@hotmail.es";
            //Toast.makeText(Registro.this,usuario,Toast.LENGTH_LONG).show();//usuario.cedula+" "+usuario.email+" "+usuario.tipo+" "+usuario.nombre,Toast.LENGTH_LONG).show();
            //EnvioAlCorreo mail=new EnvioAlCorreo();
            //mail.enviar(usuario.correo);
            //mail.enviar(usuario.email);
            if(codigo==8){
                Toast.makeText(Registro.this, "Usuario registrado con exito", Toast.LENGTH_SHORT).show();
                Login aux=new Login();
                aux.execute(inputUserName.getText().toString(), inputPassword.getText().toString());
            }
            else{
                if(codigo==33) Toast.makeText(Registro.this, "Porfavor intentelo mas tarde", Toast.LENGTH_SHORT).show();
                else if(codigo==25) Toast.makeText(Registro.this, "Revise su conexion a internet y vuelva a intentarlo", Toast.LENGTH_SHORT).show();
                else Toast.makeText(Registro.this, "Error desconocido, si el problema persiste consulte a su universidad", Toast.LENGTH_SHORT).show();
                pDialog.dismiss();
            }
            //Toast.makeText(Registro.this, "Correo enviado a "+usuario.getCorreo(), Toast.LENGTH_SHORT).show();
        }
    }

    public class Login extends AsyncTask<String,String,Usuario> {

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
        }

        @Override
        protected Usuario doInBackground(String... params) {
            final String NAMESPACE="http://tempuri.org/";
            final String METHODNAME="Autenticacion";
            //final String URL="http://192.168.1.4:8086/prueba1.asmx";
            //final String URL="http://192.168.1.130:8086/prueba1.asmx";
            final String URL=getString(R.string.ip);//casa de abuelita
            final String SOAPACTION=NAMESPACE+METHODNAME;
            Usuario recive=null;
            SoapObject request=new SoapObject(NAMESPACE, METHODNAME);
            request.addProperty("usuario",params[0]);
            request.addProperty("clave",params[1]);
            SoapSerializationEnvelope envelope=new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet=true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE transporte=new HttpTransportSE(URL);
            Log.d("Transporte ", request.toString());
            try{
                transporte.call(SOAPACTION, envelope);
                //SoapObject response=(SoapObject) envelope.bodyIn;
                SoapPrimitive response=(SoapPrimitive) envelope.getResponse();
                String strJson=response.toString();
                Gson gson=new Gson();
                recive=gson.fromJson(strJson,Usuario.class);
                /*
                List<Usuario> recive=gson.fromJson(strJson,new TypeToken<List<Usuario>>(){}.getType());
                System.out.println();
                if( recive!= null ){
                    for(Usuario object : recive){
                        System.out.println("\nAlumno : " + object.getTipo() + " " + object.getEmail() );
                    }
                }*/
                //Toast.makeText(Registro.this,strJson,Toast.LENGTH_LONG).show();
                return recive;
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (SoapFault soapFault) {
                soapFault.printStackTrace();
            } catch (HttpResponseException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e){
                e.printStackTrace();
            }
            return recive;
        }

        @Override
        protected void onPostExecute(Usuario usuarioaux) {
            super.onPostExecute(usuarioaux);
            //usuario="adrian272566@hotmail.es";
            //Toast.makeText(Registro.this,usuario,Toast.LENGTH_LONG).show();//usuario.cedula+" "+usuario.email+" "+usuario.tipo+" "+usuario.nombre,Toast.LENGTH_LONG).show();
            //mail.enviar(usuario.email);
            if(usuarioaux.equals(null))Toast.makeText(Registro.this, "Porfavor Intente mas tarde", Toast.LENGTH_SHORT).show();
            else if(usuarioaux.getCodigo()==10){
                Toast.makeText(Registro.this, "Error al iniciar sesion, verifique sus datos", Toast.LENGTH_SHORT).show();
            } else if(usuarioaux.getCodigo()==33) {
                Toast.makeText(Registro.this, "Porfavor Intente mas tarde", Toast.LENGTH_SHORT).show();
            } else{
                SharedPreferences auxLogin = getSharedPreferences("LogeoUg", MODE_PRIVATE);
                SharedPreferences.Editor edita = auxLogin.edit();
                edita.putString("nombre", usuarioaux.getNombre());
                edita.putInt("cedula", usuarioaux.getCedula());
                edita.putString("correo", usuarioaux.getCorreo());
                edita.putInt("tipo", usuarioaux.getTipo() );
                edita.commit();
                finish();
                Intent intent = new Intent(getApplicationContext(), MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);;
                startActivityForResult(intent, 0);
            }
            pDialog.dismiss();
        }
    }
}
