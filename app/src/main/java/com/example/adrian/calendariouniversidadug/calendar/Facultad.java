package com.example.adrian.calendariouniversidadug.calendar;

/**
 * Created by adrian on 27/10/17.
 */

public class Facultad {
    public int id_facultad=0;
    public int id_campus=0;
    public String nombre=null;
    public int encargado=0;

    public int getEncargado() {
        return encargado;
    }

    public void setEncargado(int encargado) {
        this.encargado = encargado;
    }

    public int getId_facultad() {
        return id_facultad;
    }

    public void setId_facultad(int id_facultad) {
        this.id_facultad = id_facultad;
    }

    public int getId_campus() {
        return id_campus;
    }

    public void setId_campus(int id_campus) {
        this.id_campus = id_campus;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
