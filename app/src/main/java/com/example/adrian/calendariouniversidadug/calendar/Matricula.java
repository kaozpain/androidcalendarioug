package com.example.adrian.calendariouniversidadug.calendar;

/**
 * Created by adrian on 08/12/17.
 */

public class Matricula {
    public int id_matricula=0;
    public int cedula=0;
    public int id_carrera=0;
    public String fecha=null;
    public String fecha_exp=null;
    public int id_materia1=0;
    public int id_materia2=0;
    public int id_materia3=0;
    public int id_materia4=0;
    public int id_materia5=0;
    public int id_materia6=0;
    public int id_materia7=0;
    public int id_materia8=0;
    public int id_materia9=0;

    public int getId_matricula() {
        return id_matricula;
    }

    public void setId_matricula(int id_matricula) {
        this.id_matricula = id_matricula;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public int getId_carrera() {
        return id_carrera;
    }

    public void setId_carrera(int id_carrera) {
        this.id_carrera = id_carrera;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getFecha_exp() {
        return fecha_exp;
    }

    public void setFecha_exp(String fecha_exp) {
        this.fecha_exp = fecha_exp;
    }

    public int getId_materia1() {
        return id_materia1;
    }

    public void setId_materia1(int id_materia1) {
        this.id_materia1 = id_materia1;
    }

    public int getId_materia2() {
        return id_materia2;
    }

    public void setId_materia2(int id_materia2) {
        this.id_materia2 = id_materia2;
    }

    public int getId_materia3() {
        return id_materia3;
    }

    public void setId_materia3(int id_materia3) {
        this.id_materia3 = id_materia3;
    }

    public int getId_materia4() {
        return id_materia4;
    }

    public void setId_materia4(int id_materia4) {
        this.id_materia4 = id_materia4;
    }

    public int getId_materia5() {
        return id_materia5;
    }

    public void setId_materia5(int id_materia5) {
        this.id_materia5 = id_materia5;
    }

    public int getId_materia6() {
        return id_materia6;
    }

    public void setId_materia6(int id_materia6) {
        this.id_materia6 = id_materia6;
    }

    public int getId_materia7() {
        return id_materia7;
    }

    public void setId_materia7(int id_materia7) {
        this.id_materia7 = id_materia7;
    }

    public int getId_materia8() {
        return id_materia8;
    }

    public void setId_materia8(int id_materia8) {
        this.id_materia8 = id_materia8;
    }

    public int getId_materia9() {
        return id_materia9;
    }

    public void setId_materia9(int id_materia9) {
        this.id_materia9 = id_materia9;
    }
}
