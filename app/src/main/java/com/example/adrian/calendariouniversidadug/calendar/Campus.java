package com.example.adrian.calendariouniversidadug.calendar;

/**
 * Created by adrian on 27/10/17.
 */
//cambiar en vs los id por id_tipo

public class Campus{
    public int id_campus=0;
    public int id_universidad=0;
    public String nombre=null;
    public int encargado=0;

    public int getEncargado() {
        return encargado;
    }

    public void setEncargado(int encargado) {
        this.encargado = encargado;
    }

    public int getId_campus() {
        return id_campus;
    }

    public void setId_campus(int id_campus) {
        this.id_campus = id_campus;
    }

    public int getId_universidad() {
        return id_universidad;
    }

    public void setId_universidad(int id_universidad) {
        this.id_universidad = id_universidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
