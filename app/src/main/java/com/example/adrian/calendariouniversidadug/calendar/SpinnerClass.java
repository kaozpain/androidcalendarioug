package com.example.adrian.calendariouniversidadug.calendar;

/**
 * Created by adrian on 26/01/18.
 */

public class SpinnerClass {
    int id=0;
    String nombre=null;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        String mensaje=nombre;
        return mensaje;
    }
}
