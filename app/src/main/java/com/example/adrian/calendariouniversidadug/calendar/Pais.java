package com.example.adrian.calendariouniversidadug.calendar;

/**
 * Created by adrian on 03/12/17.
 */

public class Pais {
    public int id_pais=0;
    public String nombre=null;

    public int getId_pais() {
        return id_pais;
    }

    public void setId_pais(int id_pais) {
        this.id_pais = id_pais;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
