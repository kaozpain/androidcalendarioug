package com.example.adrian.calendariouniversidadug;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.adrian.calendariouniversidadug.calendar.Evento;
import com.example.adrian.calendariouniversidadug.calendar.EventosSQL;
import com.example.adrian.calendariouniversidadug.calendar.SpinnerClass;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpResponseException;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.Vector;

//import android.support.v4.app.DialogFragment;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AgregarEvento.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AgregarEvento#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AgregarEvento extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public AgregarEvento() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AgregarEvento.
     */
    // TODO: Rename and change types and number of parameters
    public static AgregarEvento newInstance(String param1, String param2) {
        AgregarEvento fragment = new AgregarEvento();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_agregar_evento, container, false);
        //ExpandableListView lista2=(ExpandableListView) v.findViewById(R.id.MenuExpandibleDetallado);
        final  EditText tituloEvento=(EditText) v.findViewById(R.id.tituloEvento);
        final  EditText descripcionEvento=(EditText) v.findViewById(R.id.descripcionEvento);
        final Spinner spinner = (Spinner) v.findViewById(R.id.spinnerAgregarEvento);
        final Spinner spinnerLista = (Spinner) v.findViewById(R.id.spinnerLista);
        final Spinner spinneMateria = (Spinner) v.findViewById(R.id.spinnerMateria);
        final LinearLayout materiaLayout=(LinearLayout) v.findViewById(R.id.materiaLayout);
        final CheckBox clasesBool=(CheckBox) v.findViewById(R.id.checkClases);
        final EditText fechaFin=(EditText) v.findViewById(R.id.cambiarFechaFinEvento);
        final Button botonAceptar=(Button) v.findViewById(R.id.botonAceptar);
        final EditText fechaInicio=(EditText) v.findViewById(R.id.cambiarFechaInicioEvento);
        String diaSeleccionado= getArguments().getString("DiaSeleccionado", "");
        final String anio="20"+diaSeleccionado.substring(1, 3);
        final String mes=diaSeleccionado.substring(3, 4).replace("0","")+diaSeleccionado.substring(4, 5);
        final String dia=diaSeleccionado.substring(5, 6).replace("0","")+diaSeleccionado.substring(6, 7);
        diaSeleccionado=dia+"/"+mes+"/"+anio;
        fechaInicio.setText(diaSeleccionado);
        fechaFin.setText(diaSeleccionado);
        SharedPreferences auxLogin= this.getActivity().getSharedPreferences("LogeoUg", Context.MODE_PRIVATE);
        final Integer accesoTipo=auxLogin.getInt("tipo",0);
        final Integer accesoCedula=auxLogin.getInt("cedula",0);
        String[] permisos;
        if(accesoTipo==1){
            permisos = new String[]{"Universidad", "Campus"};
        }
        else if(accesoTipo==2){
            permisos = new String[]{"Facultad"};
        }
        else if(accesoTipo==3){
            permisos = new String[]{"Carrera"};
        }
        else{
            permisos = new String[]{"Materia"};
            materiaLayout.setVisibility(v.VISIBLE);
        }
        spinner.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, permisos));
        EventosSQL usdbh = new EventosSQL(getContext(), "DBCalendar", null, 1);
        final SQLiteDatabase db = usdbh.getWritableDatabase();

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                LinkedList<SpinnerClass> ListaSpinner2=new LinkedList<SpinnerClass>();
                Vector lst=new Vector();
                SpinnerClass spinnerAux;
                if(parent.getItemAtPosition(position).toString().equals("Universidad")){
                    Cursor cursorAgregarUniversidad = db.rawQuery("select id_universidad, nombre_universidad from ug_universidad where encargado=" + accesoCedula, null);//aqui se debe aumentar logica
                    if (cursorAgregarUniversidad != null) {
                        cursorAgregarUniversidad.moveToFirst();
                        spinnerAux=new SpinnerClass();
                        spinnerAux.setId(cursorAgregarUniversidad.getInt(0));
                        spinnerAux.setNombre(cursorAgregarUniversidad.getString(1));
                        lst.addElement(spinnerAux);
                        //llamar spinner
                    }

                    //spinnerLista.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, idUniversidadAgregar));
                }
                if(parent.getItemAtPosition(position).toString().equals("Campus")){
                    Cursor cursorAgregarCampus = db.rawQuery("select id_campus, nombre_campus from ug_campus where id_universidad=(select id_universidad from ug_universidad where encargado=" + accesoCedula+")", null);//aqui se debe aumentar logica
                    if (cursorAgregarCampus != null) {
                        cursorAgregarCampus.moveToFirst();
                        lst.removeAllElements();
                        do{
                            spinnerAux=new SpinnerClass();
                            spinnerAux.setId(cursorAgregarCampus.getInt(0));
                            spinnerAux.setNombre(cursorAgregarCampus.getString(1));
                            lst.addElement(spinnerAux);
                            //llenar spinner
                        }while (cursorAgregarCampus.moveToNext());
                        //llamar spinner
                    }
                }
                if(parent.getItemAtPosition(position).toString().equals("Facultad")){
                    Cursor cursorAgregarFacultad = db.rawQuery("select id_facultad, nombre_facultad from ug_facultad where encargado=" + accesoCedula, null);//aqui se debe aumentar logica
                    if (cursorAgregarFacultad != null) {
                        cursorAgregarFacultad.moveToFirst();
                        spinnerAux=new SpinnerClass();
                        spinnerAux.setId(cursorAgregarFacultad.getInt(0));
                        spinnerAux.setNombre(cursorAgregarFacultad.getString(1));
                        lst.addElement(spinnerAux);
                        //idUniversidadAgregar=cursorAgregarFacultad.getString(0);
                        //llamar spinner
                    }
                }
                if(parent.getItemAtPosition(position).toString().equals("Carrera")){
                    Cursor cursorAgregarCarrera = db.rawQuery("select id_carrera, nombre_carrera from ug_carrera where encargado=" + accesoCedula, null);//aqui se debe aumentar logica
                    if (cursorAgregarCarrera != null) {
                        cursorAgregarCarrera.moveToFirst();
                        spinnerAux=new SpinnerClass();
                        spinnerAux.setId(cursorAgregarCarrera.getInt(0));
                        spinnerAux.setNombre(cursorAgregarCarrera.getString(1));
                        lst.addElement(spinnerAux);
                        //idUniversidadAgregar=cursorAgregarCarrera.getString(0);
                        //llamar spinner
                    }
                }
                if(parent.getItemAtPosition(position).toString().equals("Materia")){
                    //tambien poner el curso
                    Cursor cursorAgregarCarreraCurso = db.rawQuery("select u.id_curso, u.nombre from ug_curso u, ug_materia c where u.id_curso=c.id_curso and c.cedula=" + accesoCedula, null);//aqui se debe aumentar logica

                    //Cursor cursorAgregarCarrera = db.rawQuery("select id_materia, nombre from ug_materia where cedula=" + accesoCedula, null);//aqui se debe aumentar logica
                    if (cursorAgregarCarreraCurso != null) {
                        cursorAgregarCarreraCurso.moveToFirst();
                        do{
                            spinnerAux=new SpinnerClass();
                            spinnerAux.setId(cursorAgregarCarreraCurso.getInt(0));
                            spinnerAux.setNombre(cursorAgregarCarreraCurso.getString(1));
                            lst.addElement(spinnerAux);
                            //llenar spinner
                        }while (cursorAgregarCarreraCurso.moveToNext());
                        //idUniversidadAgregar=cursorAgregarCarrera.getString(0);
                        //llamar spinner
                    }
                }
                for (int aux=0; aux<lst.size(); aux++){
                    ListaSpinner2.add((SpinnerClass) lst.elementAt(aux));
                }
                ArrayAdapter<SpinnerClass> spinner_adapter = new ArrayAdapter<SpinnerClass>(getActivity(),
                        android.R.layout.simple_spinner_item, ListaSpinner2);
                spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerLista.setAdapter(spinner_adapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerLista.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Integer idSeleccion=((SpinnerClass)parent.getItemAtPosition(position)).getId();
                if(accesoTipo==4){
                    LinkedList<SpinnerClass> ListaSpinner2=new LinkedList<SpinnerClass>();
                    Vector lst=new Vector();
                    SpinnerClass spinnerAux;
                    Cursor cursorAgregarCarreraMateria = db.rawQuery("select id_materia, nombre from ug_materia where id_curso=" + idSeleccion, null);//aqui se debe aumentar logica

                    //Cursor cursorAgregarCarrera = db.rawQuery("select id_materia, nombre from ug_materia where cedula=" + accesoCedula, null);//aqui se debe aumentar logica
                    if (cursorAgregarCarreraMateria != null) {
                        cursorAgregarCarreraMateria.moveToFirst();
                        do{
                            spinnerAux=new SpinnerClass();
                            spinnerAux.setId(cursorAgregarCarreraMateria.getInt(0));
                            spinnerAux.setNombre(cursorAgregarCarreraMateria.getString(1));
                            lst.addElement(spinnerAux);
                            //llenar spinner
                        }while (cursorAgregarCarreraMateria.moveToNext());
                        //idUniversidadAgregar=cursorAgregarCarrera.getString(0);
                        //llamar spinner
                    }
                    for (int aux=0; aux<lst.size(); aux++){
                        ListaSpinner2.add((SpinnerClass) lst.elementAt(aux));
                    }
                    ArrayAdapter<SpinnerClass> spinner_adapter = new ArrayAdapter<SpinnerClass>(getActivity(),
                            android.R.layout.simple_spinner_item, ListaSpinner2);
                    spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinneMateria.setAdapter(spinner_adapter);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        clasesBool.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    clasesBool.setText("Si hay clases");
                }
                else{
                    clasesBool.setText("No hay clases");
                }
            }
        });

        fechaFin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy");
                Date date = new Date();
                try {
                    date=formatter.parse(dia+"/"+mes+"/"+anio);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                //int year = c.get(Calendar.YEAR);
                //int month = c.get(Calendar.MONTH);
                //int day = c.get(Calendar.DAY_OF_MONTH);
                c.setTime(date);
                int day = c.get(Calendar.DAY_OF_MONTH);
                int month = c.get(Calendar.MONTH);
                int year = c.get(Calendar.YEAR);

                DatePickerDialog datePicker = new DatePickerDialog(getActivity(), AlertDialog.THEME_DEVICE_DEFAULT_DARK, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        String fecha = String.valueOf(dayOfMonth)+"/"+String.valueOf(monthOfYear+1)
                                +"/"+String.valueOf(year);
                        fechaFin.setText(fecha);

                    }
                }, year, month, day);
                DatePicker datePicker1=datePicker.getDatePicker();
                datePicker1.setMinDate(c.getTimeInMillis());
                datePicker.show();
            }
        });

        botonAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AgregarEventoSoap nuevo=new AgregarEventoSoap();
                if(tituloEvento.getText().toString()==""||tituloEvento.getText().toString().isEmpty()||tituloEvento.getText().toString()==null){
                    Toast.makeText(getActivity(), "Porfavor llenar Campo de Titulo", Toast.LENGTH_LONG).show();
                }
                else {
                    Evento aux=new Evento();
                    aux.setNombre(tituloEvento.getText().toString());
                    aux.setClases(clasesBool.isChecked()?0:1);
                    aux.setDescripcion(descripcionEvento.getText().toString().length()>0?descripcionEvento.getText().toString():"");
                    aux.setFechafin(fechaFin.getText().toString());
                    aux.setFechainicio(fechaInicio.getText().toString());
                    aux.setId_campus(spinner.getSelectedItem().toString().equals("Campus")?((SpinnerClass)spinnerLista.getSelectedItem()).getId():0);
                    aux.setId_carrera(spinner.getSelectedItem().toString().equals("Carrera")?((SpinnerClass)spinnerLista.getSelectedItem()).getId():0);
                    aux.setId_facultad(spinner.getSelectedItem().toString().equals("Facultad")?((SpinnerClass)spinnerLista.getSelectedItem()).getId():0);
                    aux.setId_materia(spinner.getSelectedItem().toString().equals("Materia")?((SpinnerClass)spinneMateria.getSelectedItem()).getId():0);
                    aux.setId_universidad(spinner.getSelectedItem().toString().equals("Universidad")?((SpinnerClass)spinnerLista.getSelectedItem()).getId():0);
                    nuevo.execute(aux);
                }
            }
        });
        return v;
    }


    public class AgregarEventoSoap extends AsyncTask<Evento,String,Integer> {

        ProgressDialog pDialog;
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Ingresando evento");
            pDialog.setCancelable(false);
            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pDialog.show();
        }

        @Override
        protected Integer doInBackground(Evento... params) {
            int codigo=25;
            final String NAMESPACE="http://tempuri.org/";
            final String METHODNAME="AgregarEvento";
            //final String URL="http://192.168.1.4:8086/prueba1.asmx";
            //final String URL="http://192.168.1.130:8086/prueba1.asmx";
            final String URL=getString(R.string.ip);
            final String SOAPACTION=NAMESPACE+METHODNAME;
            SoapObject request=new SoapObject(NAMESPACE, METHODNAME);
            request.addProperty("id_universidad",params[0].getId_universidad());
            request.addProperty("id_campus",params[0].getId_campus());
            request.addProperty("id_facultad",params[0].getId_facultad());
            request.addProperty("id_carrera",params[0].getId_carrera());
            request.addProperty("id_materia",params[0].getId_materia());
            request.addProperty("fecha_inicio",params[0].getFechainicio());
            request.addProperty("fecha_fin",params[0].getFechafin());
            request.addProperty("clases",params[0].getClases());
            request.addProperty("titulo",params[0].getNombre());
            request.addProperty("descripcion",params[0].getDescripcion());
            SoapSerializationEnvelope envelope=new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet=true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE transporte=new HttpTransportSE(URL);
            Log.d("Transporte ", request.toString());
            try{
                transporte.call(SOAPACTION, envelope);
                //SoapObject response=(SoapObject) envelope.bodyIn;

                SoapPrimitive response=(SoapPrimitive) envelope.getResponse();
                codigo=Integer.parseInt(response.toString());
                /*if(codigo==8) {
                    EnviarOTP aux=new EnviarOTP();
                    aux.execute(params[1], params[2]);
                }
                //gson=new Gson();
                //recive=gson.fromJson(strJson,Usuario.class);
                /*
                List<Usuario> recive=gson.fromJson(strJson,new TypeToken<List<Usuario>>(){}.getType());
                System.out.println();
                if( recive!= null ){
                    for(Usuario object : recive){
                        System.out.println("\nAlumno : " + object.getTipo() + " " + object.getEmail() );
                    }
                }*/
                //Toast.makeText(Registro.this,strJson,Toast.LENGTH_LONG).show();
                return codigo;
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (SoapFault soapFault) {
                soapFault.printStackTrace();
            } catch (HttpResponseException e) {
                codigo=33;
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e){
                e.printStackTrace();
            }
            return codigo;
        }

        @Override
        protected void onPostExecute(Integer codigo) {
            super.onPostExecute(codigo);
            if(codigo==25) Toast.makeText(getActivity(), "Revise su conexion a internet y vuelva a intentarlo", Toast.LENGTH_SHORT).show();
            else if(codigo==33) Toast.makeText(getActivity(), "Porfavor intentelo de nuevo mas tarde", Toast.LENGTH_SHORT).show();
            else {
                Toast.makeText(getActivity(), "Ingreso exitoso", Toast.LENGTH_SHORT).show();
                FragmentUno x = new FragmentUno();
                getFragmentManager().beginTransaction().replace(R.id.Contendedor, x).commit();
            }
            pDialog.dismiss();
            //usuario="adrian272566@hotmail.es";
            //Toast.makeText(Registro.this,usuario,Toast.LENGTH_LONG).show();//usuario.cedula+" "+usuario.email+" "+usuario.tipo+" "+usuario.nombre,Toast.LENGTH_LONG).show();
            //EnvioAlCorreo mail=new EnvioAlCorreo();
            //mail.enviar(usuario.correo);
            //mail.enviar(usuario.email);
            //Toast.makeText(Registro.this, "Correo enviado a "+usuario.getCorreo(), Toast.LENGTH_SHORT).show();
        }
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
