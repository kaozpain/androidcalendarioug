package com.example.adrian.calendariouniversidadug.calendar;

/**
 * Created by adrian on 27/10/17.
 */

public class Cursos {
    public int id_curso=0;
    public String nombre=null;
    public String modalidad=null;

    public int getId_curso() {
        return id_curso;
    }

    public void setId_curso(int id_curso) {
        this.id_curso = id_curso;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getModalidad() {
        return modalidad;
    }

    public void setModalidad(String modalidad) {
        this.modalidad = modalidad;
    }
}
