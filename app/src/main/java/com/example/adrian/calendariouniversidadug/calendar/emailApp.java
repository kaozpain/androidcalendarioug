package com.example.adrian.calendariouniversidadug.calendar;

/**
 * Created by adrian on 29/10/17.
 */

public class emailApp {
    public String correo="adrian272566@gmail.com";
    public String pass="clave";
    public String subject="Envio codigo verificacion calendarioUG";
    public String mensaje="Ingrese este codigo en la app para completar el registro "+Math.random()+" este es el codigo";
    public String formato="text/html; charset=utf-8";

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getFormato() {
        return formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }
}
